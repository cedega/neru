#SOURCE LOCATION
SOURCES=compile/examples/helloworld/main.cpp

#INCLUDE LOCATION
INCLUDE=include/
FRAMEWORKS=
CFLAGS=-O2

ifeq ($(OS),Windows_NT)
	MESSAGE="Operating System Detected: Windows"
	COMPILE=compile/windows/
	LIBS=-lneru -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -lSDL2_net -lminizip -lbz2 -lz -lm -static-libgcc -static-libstdc++ -Wall
	BINARY=binaries/windows/neru.exe
else ifeq ($(shell uname), Darwin)
	MESSAGE=“Operating System Detected: Mac OSX”
	COMPILE=compile/mac/
	LIBS=-lneru -framework SDL2 -framework SDL2_image -framework SDL2_mixer -framework SDL2_ttf -framework SDL2_net -lminizip -lz -Wall
	FRAMEWORKS="-Fcompile/mac/"
	CFLAGS += -m32
	BINARY=binaries/mac/neru
else
	MESSAGE="Operating System Detected: Linux"
	COMPILE=compile/linux/
	LIBS=-lneru -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -lSDL2_net -lminizip -lz -lm -Wall
	BINARY=binaries/linux/neru
endif

srcs = $(wildcard src/*.cpp)
objs = $(srcs:.cpp=.o)
deps = $(srcs:.cpp=.d)
LIBNAME=libneru.a
OBJECTS=src/*.o

all: lib bin

bin:
	@echo $(MESSAGE)
	g++ $(CFLAGS) $(SOURCES) -I"$(INCLUDE)" -L"$(COMPILE)" $(FRAMEWORKS) $(LIBS) -o $(BINARY)

lib: $(objs)
	@echo $(MESSAGE)
	ar rcs $(COMPILE)$(LIBNAME) $(OBJECTS)

%.o: %.cpp
	g++ $(CFLAGS) -I"$(INCLUDE)" -MMD -MP -c $< -o $@ -Wall

clean:
	rm -f src/*.o src/*.d

-include $(deps)
