Neru
=======
-------

SDL-driven 2D game engine.

Still quite volatile, so expect constant changes.

To download the data files required for compiling, please visit: https://dl.dropboxusercontent.com/u/12649758/Neru/nerudata.zip
  
Extract the files to the root of your Neru repository.

FAQ
=======
-------

Q: How do I access the documentation?

A: https://dl.dropboxusercontent.com/u/12649758/neru/html/namespacene.html.