#include <Neru/Engine.h>

class Engine : public ne::Engine
{
 public:
    Engine()
    {
        initWindow("Neru",800,600);
    }

    void events(const SDL_Event &event)
    {
        if (event.type == SDL_QUIT)
            ne::Engine::kill();
    }
    
    void logic(const float dt)
    {
        ne::Keys keyboard = ne::Input::getKeyboardState();
        if (keyboard[SDL_SCANCODE_ESCAPE])
            ne::Engine::kill();
    }
    
    void render(ne::Renderer *renderer)
    {

    }
    
 private:

};

int main(int argc, char *argv[])
{
	Engine neru;
    neru.runGameLoop();
    return EXIT_SUCCESS;
}
