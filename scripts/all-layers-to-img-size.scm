(define (all-layers-to-img-size img drawable)
	(let * 
		(
			(numlayers 0)
			(layers 0)
			(idx 0)
			(layr 0)
		)

		(set! numlayers (car (gimp-image-get-layers img)))
		(set! layers    (cadr(gimp-image-get-layers img)))
		
		(while (< idx numlayers)
			(set! layr (aref layers (- (- numlayers 1) idx)))
			(gimp-layer-resize-to-image-size layr)
			(set! idx (+ idx 1))
		)
	)
	
)		
		
(script-fu-register "all-layers-to-img-size"
	_"<Toolbox>/Xtns/Layers to Image Size"
    "Transforms all layers boundaries to the image size."
    "isaac.b.leon@gmail.com"
    "isaac.b.leon@gmail.com"
    "2014"
    "INDEXED* RGB* GRAY*"
    SF-IMAGE        "Image to use"          0
    SF-DRAWABLE     "Layer to use"          0
)
