(define (to-sprite-sheet img drawable w h s)
	(let * 
		(
			(width w)
			(height h)
			(spacing s)
			(anImage 0)
			(numlayers 0)
			(layers 0)
			(imgw 0)
			(imgh 0)
			(idx 0)
			(layr 0)
			(xoff 0)
			(yoff 0)
		)

		(set! anImage   (car (gimp-image-duplicate img)))
		(set! numlayers (car (gimp-image-get-layers anImage)))
		(set! layers    (cadr(gimp-image-get-layers anImage)))
		(set! imgw      (car (gimp-image-width anImage)))
		(set! imgh      (car (gimp-image-height anImage)))
		
		(while (< idx numlayers)
			(if (>= (+ xoff imgw) width) (set! yoff (+ yoff imgh spacing)))
			(if (>= (+ xoff imgw) width) (set! xoff 0))

			(set! layr (aref layers (- (- numlayers 1) idx)))
			(gimp-item-set-visible layr 1)
			(gimp-layer-resize-to-image-size layr)
			(gimp-layer-translate layr xoff yoff)
			(set! xoff (+ imgw xoff spacing))
			(set! idx (+ idx 1))
		)
		
		(gimp-image-resize-to-layers anImage)
		(gimp-image-merge-visible-layers anImage EXPAND-AS-NECESSARY)
		(gimp-image-resize anImage width height spacing spacing)

		(gimp-display-new anImage)
	)
	
)		
		
(script-fu-register "to-sprite-sheet"
	_"<Toolbox>/Xtns/To Sprite-Sheet..."
    "Creates a SpriteSheet of a certain size, with a certain spacing on the edges and inbetween frames."
    "isaac.b.leon@gmail.com & Brian.Schultheiss@airegear.com (original)"
    "isaac.b.leon@gmail.com & Brian.Schultheiss@airegear.com (original)"
    "2014"
    "INDEXED* RGB* GRAY*"
    SF-IMAGE        "Image to use"          0
    SF-DRAWABLE     "Layer to use"          0
	SF-VALUE		"Width"					"1024"
	SF-VALUE		"Height"				"1024"
	SF-VALUE		"Spacing"				"2"
)
