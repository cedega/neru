#! /bin/bash

# ============================
# Creates a new Neru project
# ============================
# Version 1.1

DIR=$(dirname $0)

if [[ "$DIR" != "." ]]; then
    echo "Please run the script from the Neru root folder."
    exit 1
fi

if [[ -z "$1" ]]; then
    echo "Please supply a project name as the first argument."
    exit 1
fi

# Create Necessary Directories
mkdir -p ../$1/src
mkdir -p ../$1/binaries/windows
mkdir -p ../$1/binaries/linux/lib64
mkdir -p ../$1/binaries/mac/frameworks

# Sample main file
cp -r compile/examples/helloworld/main.cpp ../$1/src

# Linux
cp -r binaries/linux/lib64/*.so* ../$1/binaries/linux/lib64/
cp -r binaries/linux/neru.sh ../$1/binaries/linux/

# Windows
cp -r binaries/windows/*.dll ../$1/binaries/windows/

# Mac
cp -r binaries/mac/frameworks/* ../$1/binaries/mac/frameworks/
cp -r binaries/mac/neru.sh ../$1/binaries/mac/

# Configs
cp .gitignore ../$1/
cp compile/Makefile-newproject ../$1/Makefile

echo "Project \"$1\" finished."
