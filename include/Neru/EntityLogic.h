#pragma once
#include <iostream>
#include <cfloat>
#include "Tools.h"
#include "Copy.h"

namespace ne
{
    enum { MAX_FORCES = 8 };
    
    struct Force
    {
        bool active;
        bool rerun;
        float velocity;
        float acceleration;
        int time;
        bool disable_gravity;

        Force()
        {
            active = false;
            rerun = true;
            velocity = 0;
            acceleration = 0;
            time = 0;
            disable_gravity = false;
        }

        Force(float vel, float acc, int t)
        {
            active = false;
            rerun = true;
            velocity = vel;
            acceleration = acc;
            time = t;
            disable_gravity = false;
        }
    };
};

namespace ne
{

class Engine;

//! Euler integration movement, collision, and forces.

//!----------------------
//! Overview
//!----------------------
//!
//! Handles hitbox creation, collision, integration, and forces.
class EntityLogic
{
 public:
    friend class Engine;
    EntityLogic();

    //!
    //! Integrates the EntityLogic's movement and updates the position.
    //!
    //! <b>rerun</b> dictates if the integration is to be done at the "catch-up" phase of a network game.
    //! if the game is not being played over a network, leave <b>rerun</b> as false.
    //!
    //! <b>NOTE: As of the current version, network play is not supported. "rerun" will be useless, so leave it alone.</b>
    void move(const float dt, bool rerun=false);
    //!
    //! Applies basic collision to two rectangles.
    //! Rectangle <b>A</b> will be the one displaced or move, and <b>B</b> will never move.
    //! Returns an integer between 0-6 which represents what section of <b>B</b> was collided against.
    //! 0 = none, 1 = on right, 2/5 = on top, 3/6 = on bottom, 4 = on left.
    int collide(ne::Rectf *A, const ne::Rectf *B);
     
    // Forces
    
    //!
    //! Creates a new X-axis force. <b>velocity</b> is applied for <b>time</b> amount of milliseconds.
    int addForceX(float velocity, float acceleration, int time);
    //!
    //! Creates a new Y-axis force. <b>velocity</b> is applied for <b>time</b> amount of milliseconds.
    int addForceY(float velocity, float acceleration, int time);
    //!
    //! Creates a new X-axis force by taking an acutal force construct.
    int addForceX(const ne::Force &force);
    //!
    //! Creates a new Y-axis force by taking an acutal force construct.
    int addForceY(const ne::Force &force);
    //!
    //! Removes the X-axis force at index <b>force_id</b>.
    void removeForceX(int force_id);
    //!
    //! Removes the Y-axis force at index <b>force_id</b>.
    void removeForceY(int force_id);
    //!
    //! Removes all forces.
    void removeAllForces();
    //!
    //! Returns whether a X-axis force at <b>force_id</b> exists.
    bool forceExistsX(int force_id) const;
    //!
    //! Returns whether a Y-axis force at <b>force_id</b> exists.
    bool forceExistsY(int force_id) const;
    //!
    //! Returns whether at least one X-axis force is present.
    bool anyForceExistsX() const;
    //!
    //! Returns whether at least one Y-axis force is present.
    bool anyForceExistsY() const;
    //!
    //! Returns whether a force has triggered gravity to temporarily no longer apply.
    bool forceHasDisabledGravity() const;
    //!
    //! Takes an index of a X-axis force and disables the act of using it in the "catch-up" step in network games.
    //! Returns the same <b>force_id</b> passed.
    //!
    //! <b>NOTE: As of the current version, network play is not supported. "rerun" will be useless, so leave it alone.</b>
    int applyNoRerunX(int force_id);
    //!
    //! Takes an index of a Y-axis force and disables the act of using it in the "catch-up" step in network games.
    //! Returns the same <b>force_id</b> passed.
    //!
    //! <b>NOTE: As of the current version, network play is not supported. "rerun" will be useless, so leave it alone.</b>
    int applyNoRerunY(int force_id);
    //!
    //! Takes an index of a X-axis force and disables all gravity affecting the EntityLogic for the force's duration.
    //! Returns the same <b>force_id</b> passed.
    int applyNoGravityX(int force_id);
    //!
    //! Takes an index of a Y-axis force and disables all gravity affecting the EntityLogic for the force's duration.
    //! Returns the same <b>force_id</b> passed.
    int applyNoGravityY(int force_id);

    // Triggers
    
    //!
    //! Executes every time an X-axis force ends.
    //! Passes a <b>force_id</b>, which is the index of the X-axis force.
    virtual void onForceXEnd(int force_id);
    //!
    //! Executes every time an Y-axis force ends.
    //! Passes a <b>force_id</b>, which is the index of the Y-axis force.
    virtual void onForceYEnd(int force_id);
    //!
    //! Executes every time the EntityLogic hits the ground.
    virtual void onLand();

    // Static
    
    //!
    //! Sets the default gravity used for all entities.
    static void setDefaultGravity(float); 
    //!
    //! Sets the default terminal X & Y velocity used for all entities.
    static void setDefaultTerminalVelocity(const ne::Vector2f &terminal_velocity);
    //!
    //! Returns the default gravity used for all entities.
    static float getDefaultGravity();
    //!
    //! Returns the default terminal velocity used for all entities.
    static ne::Vector2f getDefaultTerminalVelocity();

    // Setters
    
    //!
    //! Sets the width and height of the hitbox.
    void setHitbox(const ne::Vector2f &h);
    void setX(float);
    void setY(float);
    void setPosition(const ne::Vector2f &position);
    void setVelocity(const ne::Vector2f &velocity);
    void setAcceleration(const ne::Vector2f &acceleration);
    //!
    //! Sets whether gravity applies to this EntityLogic.
    void setIgnoreGravity(bool);
    void setGravity(float);
    void setTerminalVelocity(const ne::Vector2f &terminal_velocity);

    // Getters

    //!
    //! Returns the width and height of the hitbox.
    const ne::Rectf& getHitbox() const;
    float getX() const;
    float getY() const;
    ne::Vector2f getPosition() const; 
    ne::Vector2f getCentre() const;
    const ne::Vector2f& getVelocity() const;
    //!
    //! Returns the amount of movement done over a single move() call.
    const ne::Vector2f& getStep() const;
    const ne::Vector2f& getAcceleration() const;
    //!
    //! Returns the total combined force velocity applied to the EntityLogic.
    const ne::Vector2f& getForceVelocity() const;
    bool getIgnoreGravity() const;
    float getGravity() const;
    const ne::Vector2f& getTerminalVelocity() const;

 private:
    DISALLOW_COPY_AND_ASSIGN(EntityLogic);

    static float default_gravity_;
    static ne::Vector2f default_terminal_velocity_;
    static float physics_dt_;

    ne::Rectf hitbox_;
    ne::Vector2f velocity_;
    ne::Vector2f acceleration_;
    ne::Vector2f force_velocity_;
    ne::Vector2f step_;

    bool ignore_gravity_;
    float gravity_;
    ne::Vector2f terminal_velocity_;

    ne::Force forces_x_[ne::MAX_FORCES];
    ne::Force forces_y_[ne::MAX_FORCES];
    int num_forces_x_;
    int num_forces_y_;

    void applyForces(float dt, bool rerun=false);

    //! Sets the physics delta time. Called by ne::Engine.
    static void setPhysicsDt(float dt);
};

}
