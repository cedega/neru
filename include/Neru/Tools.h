#pragma once
#include "SDL2/SDL_rect.h"
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <ctime>

#define PI 3.14159265f
#define DEGTORAD 0.0174532925199432957f
#define RADTODEG 57.295779513082320876f

namespace ne
{

struct Rectf
{
    float x;
    float y;
    float w;
    float h;
    
    Rectf()
    {
        x = 0.0f;
        y = 0.0f;
        w = 0.0f;
        h = 0.0f;
    }
	
    Rectf(float x2, float y2, float w2, float h2)
    {
        x = x2;
        y = y2;
        w = w2;
        h = h2;
    }
};

struct Recti
{
    int x;
    int y;
    int w;
    int h;
    
    Recti()
    {
        x = 0;
        y = 0;
        w = 0;
        h = 0;
    }
	
    Recti(int x2, int y2, int w2, int h2)
    {
        x = x2;
        y = y2;
        w = w2;
        h = h2;
    }
};

struct Vector2f
{
    float x;
    float y;
    
    Vector2f()
    {
        x = 0;
        y = 0;
    }
    
    Vector2f(float x2, float y2)
    {
        x = x2;
        y = y2;
    }
    
    Vector2f(const Vector2f &vector)
    {
        x = vector.x;
        y = vector.y;
    }
    
    Vector2f& operator+=(const Vector2f &right)
    {
        x += right.x;
        y += right.y;
        return *this;
    }
    
    Vector2f& operator-=(const Vector2f &right)
    {
        x -= right.x;
        y -= right.y;
        return *this;
    }
    
    Vector2f& operator*=(float right)
    {
        x *= right;
        y *= right;
        return *this;
    }
    
    Vector2f& operator/=(float right)
    {
        x /= right;
        y /= right;
        return *this;
    }

    float length()
    {
        return std::sqrt(x*x + y*y);
    }

    void normalize()
    {
        float l = length();

        if (l > 0)
        {
            x /= l;
            y /= l;
        }
        else
        {
            x = 0;
            y = 0;
        }
    }
};

inline Vector2f operator+(Vector2f left, const Vector2f &right)
{
    left.x += right.x;
    left.y += right.y;
    return left;
}
    
inline Vector2f operator-(Vector2f left, const Vector2f &right)
{
    left.x -= right.x;
    left.y -= right.y;
    return left;
}

inline Vector2f operator*(float left, Vector2f right)
{
    right.x *= left;
    right.y *= left;
    return right;
}

inline Vector2f operator*(Vector2f left, float right)
{
    left.x *= right;
    left.y *= right;
    return left;
}

inline Vector2f operator/(Vector2f left, float right)
{
    left.x /= right;
    left.y /= right;
    return left;
}

struct Vector2i
{
    int x;
    int y;
    
    Vector2i()
    {
        x = 0;
        y = 0;
    }
    
    Vector2i(int x2, int y2)
    {
        x = x2;
        y = y2;
    }
    
    Vector2i(const Vector2i &vector)
    {
        x = vector.x;
        y = vector.y;
    }
    
    Vector2i& operator+=(const Vector2i &right)
    {
        x += right.x;
        y += right.y;
        return *this;
    }
    
    Vector2i& operator-=(const Vector2i &right)
    {
        x -= right.x;
        y -= right.y;
        return *this;
    }
    
    Vector2i& operator*=(int right)
    {
        x *= right;
        y *= right;
        return *this;
    }
    
    Vector2i& operator/=(int right)
    {
        x /= right;
        y /= right;
        return *this;
    }

    float length()
    {
        return std::sqrt(x*x + y*y);
    }
};

inline Vector2i operator+(Vector2i left, const Vector2i &right)
{
    left.x += right.x;
    left.y += right.y;
    return left;
}
    
inline Vector2i operator-(Vector2i left, const Vector2i &right)
{
    left.x -= right.x;
    left.y -= right.y;
    return left;
}

inline Vector2i operator*(int left, Vector2i right)
{
    right.x *= left;
    right.y *= left;
    return right;
}

inline Vector2i operator*(Vector2i left, int right)
{
    left.x *= right;
    left.y *= right;
    return left;
}

inline Vector2i operator/(Vector2i left, int right)
{
    left.x /= right;
    left.y /= right;
    return left;
}

struct Vector3i
{
    int x;
    int y;
    int z;
    
    Vector3i()
    {
        x = 0;
        y = 0;
        z = 0;
    }
    
    Vector3i(int x2, int y2, int z2)
    {
        x = x2;
        y = y2;
        z = z2;
    }
    
    Vector3i(const Vector3i &vector)
    {
        x = vector.x;
        y = vector.y;
        z = vector.z;
    }
    
    Vector3i& operator+=(const Vector3i &right)
    {
        x += right.x;
        y += right.y;
        z += right.z;
        return *this;
    }
    
    Vector3i& operator-=(const Vector3i &right)
    {
        x -= right.x;
        y -= right.y;
        z -= right.z;
        return *this;
    }
    
    Vector3i& operator*=(int right)
    {
        x *= right;
        y *= right;
        z *= right;
        return *this;
    }
    
    Vector3i& operator/=(int right)
    {
        x /= right;
        y /= right;
        z /= right;
        return *this;
    }

    float length()
    {
        return std::sqrt(x*x + y*y + z*z);
    }
};

inline Vector3i operator+(Vector3i left, const Vector3i &right)
{
    left.x += right.x;
    left.y += right.y;
    left.z += right.z;
    return left;
}
    
inline Vector3i operator-(Vector3i left, const Vector3i &right)
{
    left.x -= right.x;
    left.y -= right.y;
    left.z -= right.z;
    return left;
}

inline Vector3i operator*(int left, Vector3i right)
{
    right.x *= left;
    right.y *= left;
    right.z *= left;
    return right;
}

inline Vector3i operator*(Vector3i left, int right)
{
    left.x *= right;
    left.y *= right;
    left.z *= right;
    return left;
}

inline Vector3i operator/(Vector3i left, int right)
{
    left.x /= right;
    left.y /= right;
    left.z /= right;
    return left;
}

struct Vector3f
{
    float x;
    float y;
    float z;
    
    Vector3f()
    {
        x = 0;
        y = 0;
        z = 0;
    }
    
    Vector3f(float x2, float y2, float z2)
    {
        x = x2;
        y = y2;
        z = z2;
    }
    
    Vector3f(const Vector3f &vector)
    {
        x = vector.x;
        y = vector.y;
        z = vector.z;
    }
    
    Vector3f& operator+=(const Vector3f &right)
    {
        x += right.x;
        y += right.y;
        z += right.z;
        return *this;
    }
    
    Vector3f& operator-=(const Vector3f &right)
    {
        x -= right.x;
        y -= right.y;
        z -= right.z;
        return *this;
    }
    
    Vector3f& operator*=(float right)
    {
        x *= right;
        y *= right;
        z *= right;
        return *this;
    }
    
    Vector3f& operator/=(float right)
    {
        x /= right;
        y /= right;
        z /= right;
        return *this;
    }

    float length()
    {
        return std::sqrt(x*x + y*y + z*z);
    }

    void normalize()
    {
        float l = length();

        if (l > 0)
        {
            x /= l;
            y /= l;
            z /= l;
        }
        else
        {
            x = 0;
            y = 0;
            z = 0;
        }
    }

};

inline Vector3f operator+(Vector3f left, const Vector3f &right)
{
    left.x += right.x;
    left.y += right.y;
    left.z += right.z;
    return left;
}
    
inline Vector3f operator-(Vector3f left, const Vector3f &right)
{
    left.x -= right.x;
    left.y -= right.y;
    left.z -= right.z;
    return left;
}

inline Vector3f operator*(float left, Vector3f right)
{
    right.x *= left;
    right.y *= left;
    right.z *= left;
    return right;
}

inline Vector3f operator*(Vector3f left, float right)
{
    left.x *= right;
    left.y *= right;
    left.z *= right;
    return left;
}

inline Vector3f operator/(Vector3f left, float right)
{
    left.x /= right;
    left.y /= right;
    left.z /= right;
    return left;
}

// Functions

//!
//! Converts a boolean into a string.
std::string toString(bool boolean);
//!
//! Converts a character into a string.
std::string toString(char character);
//!
//! Converts a float into a string.
std::string toString(float number);
//!
//! Converts an int into a string.
std::string toString(int number);
//!
//! Converts a double into a string.
std::string toString(double number);
//!
//! Converts a long into a string.
std::string toString(long number);
//!
//! Converts a ne::Vector2f into a string.
std::string toString(const ne::Vector2f &vector);
//!
//! Converts a ne::Vector2i into a string.
std::string toString(const ne::Vector2i &vector);
//!
//! Checks for the existance of a file on storage
bool fileExists(const std::string &path);
//!
//! Float absolute value function
float absval(float number);
//!
//! Converts a whole-number from a decimal based on rounding.
int round(float num);
//!
//! Returns a random number between and including min to max
int random(int min, int max);
//!
//! Returns a random float number between and including min to max.
//! The third argument is the degree of precision. For example, 100 is 2 decimal places.
float random(float min, float max, int precision=1000);
//!
//! Intersection between 2 float rectangles.
bool intersects(const ne::Rectf &A, const ne::Rectf &B);
//!
//! Intersection between 2 integer rectangles.
bool intersects(const ne::Recti &A, const ne::Recti &B);
//!
//! Intersection between 2 lines.  The last argument is the output intersection point.
bool intersects(const ne::Vector2f &A1, const ne::Vector2f &A2, const ne::Vector2f &B1, const ne::Vector2f &B2, ne::Vector2f *intersection);
//!
//! Intersection between a line and a square.  The last argument is the distance to the intersection point.
bool intersects(const ne::Vector2f &ray_position, ne::Vector2f ray_direction, const ne::Rectf &square, float *distance);
//!
//! Conversion to ne::Recti
ne::Recti toRect(const SDL_Rect &rect);
//!
//! Conversion to SDL_Rect
SDL_Rect toSDLRect(const ne::Recti &rect);
//!
//! Conversion to SDL_Rect from ints.
SDL_Rect toSDLRect(int x, int y, int w, int h);
//!
//! Conversion to SDL_Color
SDL_Color toSDLColor(int r, int g, int b, int a = 255);

}
