#pragma once
#include "TiledParser.h"
#include "SpriteSheet.h"

namespace ne
{

class Renderer;

//! Extends ne::TiledParser to add drawing support.

//!----------------------
//! Overview
//!----------------------
//!
//! Loads a texture and a TMX (CSV type) Tiled file. Is renderable through ne::Render::draw().
class TileMap : public TiledParser
{
 public:
    friend class Renderer;
    TileMap();
    //!
    //! Loads a .TMX (CSV) file through <b>map_file</b>, and loads an on-disk texture through <b>texture_path</b>
    void loadFromFile(const std::string &map_file, const std::string &texture_path);
    //!
    //! Loads a .TMX (CSV) file and texture from the zip data file.
    void loadFromZip(const std::string &map_file, const std::string &texture_path);
    
    // Getters
    
    //!
    //! Returns the address to the texture loaded by loadFromFile()/loadFromZip.
    SDL_Texture* getTexture();
    //!
    //! Returns the texture rectangle, which should be the coordinates of a tile inside the tilemap.
    SDL_Rect* getTextureRect();
    //!
    //! Returns the entire tilemap size in pixels.
    ne::Vector2i getPixelSize() const;
    //!
    //! Returns the internal ne::SpriteSheet.
    ne::SpriteSheet* getSpriteSheet();

 private:
    DISALLOW_COPY_AND_ASSIGN(TileMap);
    ne::SpriteSheet texture_;

    //!
    //! Sets the tile to be rendered by invoking ne::SpriteSheet::setSprite().
    void setRenderTile(int tile_id);
};

}
