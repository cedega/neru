#pragma once
#include "SDL2/SDL_ttf.h"
#include "Zip.h"
#include "Tools.h"

namespace ne
{

class Renderer;

//! Loads and draws font files.

//!----------------------
//! Overview
//!----------------------
//!
//! A class which displays fonts using SDL_ttf.
class Text
{
 public:
    friend class Renderer;
    Text();
    ~Text();

    //!
    //! Loads a font from the zip file defined in ne::Engine at the path <b>file_path</b>.
    void loadZipFont(const std::string &file_path, int character_size=16);
    //!
    //! Loads a font regularly from an on-disk file at path <b>file_path</b>.
    void loadFileFont(const std::string &file_path, int character_size=16);
    
    //!
    //! Updates the internal last text positon to the current one. Used for interpolation, and should be called
    //! right before moving the text.
    void updateLast();
    //!
    //! Updates the internal last text position to the specified (<b>x</b>,<b>y</b>) position.
    void updateLast(float x, float y);
    //!
    //! Updates the internal last text position to the vector specified in <b>last_position</b>.
    void updateLast(const ne::Vector2f &last_position);

    // Setters

    //!
    //! Sets whether or not interpolation will be applied to the text.
    void useInterpolation(bool);
    //!
    //! Sets a font to use rather than loading one.
    void setFont(TTF_Font *font);
    //!
    //! Sets the position of the text.
    void setPosition(const ne::Vector2f &position);
    //!
    //! Sets the scale of the text.
    void setScaling(const ne::Vector2f &scale);
    //!
    //! Sets the x-coordinate of the text.
    void setX(float);
    //!
    //! Sets the y-coordinate of the text.
    void setY(float);
    //!
    //! Sets the string.  This is an expensive operation, which has to create a new surface and texture.
    void setString(const std::string &string);
    //!
    //! Sets the colour of the text.
    void setColour(const SDL_Color &colour);
    //!
    //! Sets the amount of pixels to wrap the text around.
    void setWrap(unsigned int wrap);
        
    // Getters
    
    //!
    //! Returns whether the text is being interpolated.
    bool isInterpolated() const;
    //!
    //! Returns the loaded font.
    TTF_Font* getFont();
    //!
    //! Returns the SDL_Texture for the text.
    SDL_Texture* getTexture();
    //!
    //! Returns the position of the text.
    const ne::Vector2f& getPosition() const;
    //!
    //! Returns the scale of the text.
    const ne::Vector2f& getScaling() const;
    //!
    //! Returns the text dimension.
    SDL_Rect* getTargetRect();
    //!
    //! Returns the texture sub-rectangle of the text.
    SDL_Rect* getTextureRect();
    //!
    //! Returns the character size of the font.
    int getCharacterSize() const;
    //!
    //! Returns the current x-coordinate of the text.
    float getX() const;
    //!
    //! Returns the current y-coordinate of the text.
    float getY() const;
    //!
    //! Returns the width of the texture which represents the text.
    int getW() const;
    //!
    //! Returns the height of the texture which represents the text.
    int getH() const;
    //!
    //! Returns the internal last position recorded by updateLastPosition() (or its varieties)
    const ne::Vector2f& getLastPosition() const;
    //!
    //! Returns the calculated interpolation position, given a <b>interpolation</b>.
    ne::Vector2f getInterpolatedPosition(float interpolation) const;
    //!
    //! Returns the current string.
    const std::string& getString() const;
    //!
    //! Returns the colour of a string.
    const SDL_Color& getColour() const;
    //!
    //! Returns how many pixels to wrap the text around.
    unsigned int getWrap() const;
    
 private:
    DISALLOW_COPY_AND_ASSIGN(Text);

    static SDL_Renderer *internal_renderer_;
    
    TTF_Font *font_;
    SDL_Texture *texture_;
    SDL_RWops *zip_rw_;
    SDL_Rect target_rect_;
    SDL_Rect texture_rect_;
    SDL_Color colour_;

    bool font_loaded_;
    int size_;

    bool use_interpolation_;
    ne::Vector2f position_;
    ne::Vector2f last_position_;
    ne::Vector2f scale_;
    
    std::string string_;
    unsigned int wrap_;
    
    // Keeps track of zip memory data to free up later
    char* zip_font_buffer_;
    bool zip_font_loaded_;

    void updateTarget();
    void deconstruct();

    //!
    //! Sets the internal renderer to be used when translating SDL_Surfaces to SDL_Textures.
    //! Called by ne::Renderer.
    static void setInternalRenderer(SDL_Renderer *renderer);
};

}
