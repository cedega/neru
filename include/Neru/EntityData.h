#pragma once
#include <vector>
#include <map>
#include "SpriteSheet.h"

namespace ne
{

//! A ne::SpriteSheet to use in ne::EntityImage.

//!----------------------
//! Overview
//!----------------------
//!
//! Handles spritesheet loading and animating.
//!
//! Is loaded by a ne::EntityImage.
//!
//! The term <b>linear frame</b> defined in this documentation refers to the reading of frames in a linear manner, rather than <b>(row, column)</b>.
//!
//! Example:
//!
//! |    |    |    |    |    |
//! | -- | -- | -- | -- | -- |
//! | 1  | 2  | 3  | 4  | 5  |
//! | 6  | 7  | 8  | 9  | 10 |
//! | 11 | 12 | 13 | 14 | 15 |
//!
//! So a range defined by 6 -> 10 will take all of row #2.
//!
//!----------------------
//!
//! Conversely, the term <b>normalized frame</b> refers to the actual frame number of an animation.
//!
//! Example:
//!
//! Given a range of linear frames from 6 -> 10, 6 would be the first frame of the animaton.  Therefore, frame 1 is the "normalized frame" of 6.
class EntityData : public ne::SpriteSheet
{
 public:
    EntityData();
    //!
    //! <b>spritesheet_frame_width</b> refers to the pixel width of a single frame inside the spritesheet.
    //! <b>spritesheet_frame_height</b> refers to the pixel height of a single frame inside the spritesheet.
    //! <b>spritesheet_frame_spacing</b> refers to the horizontal and vertical pixel spacing between frames.
    EntityData(int spritesheet_frame_width, int spritesheet_frame_height, int spritesheet_frame_spacing);

    //!
    //! Creates a new animation inside the spritesheet.
    //! <b>animation_name</b> defines the name of the animation. This is used to load an animation inside a ne::EntityImage.
    //! <b>start_frame</b> and <b>end_frame</b> define a range of linear frames to be used in the animation.
    //! <b>timing</b> is the timing for every frame in milliseconds. Note that per-frame timings can be customized with setFrameTiming().
    void newAnimation(const std::string &animation_name, int start_frame, int end_frame, int timing=0);

    //!
    //! Returns the row which the linear frame is on.
    int convertToRow(int frame) const;
    //!
    //! Returns the column which the linear frame is on.
    int convertToColumn(int frame) const;
    //!
    //! Returns the linear frame based on the animation, and the normalized frame passed.
    //! This function bridges the gap between this class and ne::Animation, because ne::Animation only uses normalized frames.
    int convertToFrame(const std::string &animation_name, int normalized_frame) const;

    //!
    //! Checks if the string <b>animation_name</b> exists as a previously defined animation.
    //! If it exists, this function returns true, and if it doesn't it returns false.
    //! <b>index</b> is set to the index of the found animation.
    bool animationIsValid(const std::string &animation_name, int *index) const;
    //!
    //! Returns whether the linear frame <b>frame</b> is valid for an animation at <b>animation_index</b>.
    //! Note that no validity checking is done on <b>animation_index</b>.
    bool frameIsValid(int frame, int animation_index) const;
    //!
    //! Returns whether the normalized frame <b>nframe</b> is valid for an animation at <b>animation_index</b>.
    //! Note that no validity checking is done on <b>animation_index</b>.
    bool nframeIsValid(int nframe, int animation_index) const;

    // Setters

    //!
    //! Sets a displacement to be used across all frames and all animations.
    void setBaseDisplacement(int x, int y, int hitbox_width_of_entity); 
    //!
    //! Sets the displacement for the last-defined animation via the newAnimation() function.
    void displaceAnimation(int x, int y);
    //!
    //! Sets the displacement of a specific linear frame for the last-defined animation via the newAnimation() function.
    void displaceFrame(int frame, int x, int y);

    //!
    //! Sets the timing in milliseconds of all frames for the last-defined animation via the newAnimation() function.
    void setAnimationTiming(int timing);
    //!
    //! Sets the timing in milliseconds of a specific linear frame for the last-defined animation via the newAnimation() function.
    void setFrameTiming(int frame, int timing);
    //!
    //! Sets whether the animation will loop after completing for the last-defined animation via the newAnimation function.
    void setLoop(bool);

    //!
    //! Defines a range of <b>linear frames</b> which the animation is deemed "active" for the last-defined animation via the newAnimation() function. 
    //! This is for testing if an animation has triggered its "action point."
    //! Can be used for anything from attack hitboxing to self-applied spells. 
    void setActiveFrames(int frame_start, int frame_end);
    //!
    //! Defines the active rectangular hitbox for a given <b>linear frame</b> for the last-defined animation via the newAnimation() function.
    //! Must be in the range of setActiveFrames() to be useful.
    //! This function is completely optional, but is highly useful for defining hitboxes for attacks.
    //! For example, for a self-healing ability, a programmer might leave setActiveRegion() undefined, and only test if the spell applies via the isActive() function.
    void setActiveRegion(int frame, const ne::Rectf &region);

    // Getters

    bool animationExists(const std::string &animation_name) const;
    int getAnimationIndex(const std::string &animation_name) const;

    //!
    //! Returns the displacement that is applied to all frames on every animation.
    const ne::Vector3i& getBaseDisplacement() const;
    //!
    //! Returns the displacement of a specific normalized frame on the defined animation <b>animation_name</b>.
    ne::Vector2i getFrameDisplacement(const std::string &animation_name, int nframe) const;
    //!
    //! Returns the timing in milliseconds of a specific normalized frame on the defined animation <b>animation_name</b>.
    int getFrameTiming(const std::string &animation_name, int nframe) const;
    //!
    //! Returns whether the animation will loop after completing.
    bool getLoop(const std::string &animation_name) const;

    unsigned int getNumberOfFrames(const std::string &animation_name) const;
    //!
    //! Returns the linear frame range of the defined animation <b>animation_name</b>.
    ne::Vector2i getFrameRange(const std::string &animation_name) const; 

    //!
    //! Returns whether an animation is on its defined "active range" based on the supplied normalized frame <b>nframe</b>.
    bool isActive(const std::string &animation_name, int nframe) const;
    //!
    //! Returns the linear frame range of the defined "active range" based on the defined animation <b>animation_name</b>.
    ne::Vector2i getActiveRange(const std::string &animation_name) const;
    //!
    //! Returns the hitbox of a normalized active frame <b>nframe</b> of the defined animation <b>animation_name</b>.
    ne::Rectf getActiveRegion(const std::string &animation_name, int nframe) const;

 private:
    DISALLOW_COPY_AND_ASSIGN(EntityData);

    struct AnimationData
    {
        bool loop;
        ne::Vector2i range;
        ne::Vector2i active_range;
        std::vector<int> timing;
        std::vector<ne::Vector2i> frame_displacement;
        std::vector<ne::Rectf> active_region;
    };

    // Map to associate animation names with vector data
    std::map<std::string,int> name_map_;

    // Data of Animations
    std::vector<AnimationData> animation_;

    ne::Vector3i base_displacement_;
    int last_created_animation_;
};

}
