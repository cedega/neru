#pragma once
#include "SDL2/SDL_mixer.h"
#include "Zip.h"

namespace ne
{

//! Plays music.

//!----------------------
//! Overview
//!----------------------
//!
//! Loads music from a zip file or on-disk file.
class Music
{
 public:
    Music();
    ~Music();

    //!
    //! Loads a music file from within the zip file specified in ne::Engine.
    void loadZipMusic(const std::string &file_path);
    //!
    //! Loads a regular music file from the disk.
    void loadFileMusic(const std::string &file_path);
    
    //!
    //! Plays the music, which will loop if left default.
    void play(int loop=-1);
    //!
    //! Stops all music, from all instances of ne::Music.
    static void stop();
    //!
    //! Stops all music after fading it out for the passed milliseconds.
    static void fadeOut(int ms);
    //!
    //! Plays the music, but fades in the volume for the passed milliseconds.
    void fadeIn(int ms, int loops=-1);
    //!
    //! Returns whether music is playing or not.
    static bool playing();
    
    // Setters
    void setVolume(int volume);
    
    // Getters
    int getVolume() const;
    const std::string& getFileName() const;
    
 private:
    DISALLOW_COPY_AND_ASSIGN(Music);

    Mix_Music *music_;
    SDL_RWops *zip_rw_;
    char *zip_music_buffer_;
    bool zip_used_;

    std::string file_name_;
    
    int volume_;

    void deconstruct();
};

}
