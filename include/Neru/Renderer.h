#pragma once
#include "Camera.h"
#include "EntityImage.h"
#include "ImageAtlas.h"
#include "TileMap.h"
#include "Text.h"

namespace ne
{

//! Draws graphics and other Neru data types.

//!----------------------
//! Overview
//!----------------------
//!
//! ne::Renderer is an object passed by reference through the ne::Engine's gameloop.
//!
//! It primarily handles interpolated drawing between all drawable Neru classes.
class Renderer
{
 public:
    Renderer();
    
    //!
    //! Binds the Renderer to a window.
    bool bindWindow(SDL_Window *window);

    //!
    //! Draws a ne::Image.
    void draw(ne::Image &image);
    //!
    //! Draws a ne::ImageAtlas. The <b>atlas_id</b> is the index of the texture rectangle added within ne::TextureAtlas::add().
    void draw(ne::ImageAtlas &atlas, int atlas_id);
    //!
    //! Draws a ne::Text.
    void draw(ne::Text &text);
    //!
    //! Draws a ne::EntityImage, which is the currently "active" ne::EntityData.
    void draw(ne::EntityImage &entity_image);
    //!
    //! Draws a ne::TileMap.
    void draw(ne::TileMap &tile_map, int layer);
    //!
    //! Draws a rectangle as a black-outlined box.
    void draw(ne::Rectf rect, const SDL_Color &colour);
    //!
    //! Draws a 1 pixel line between point1 and point2.
    void drawLine(const ne::Vector2f &point1, const ne::Vector2f &point2, const SDL_Color &colour);

    // Setters
    
    //!
    //! Sets the interpolation value to be used for interpolation.
    //! Called mainly by ne::Engine.
    void setInterpolationValue(float interpolation);
    //!
    //! Sets the camera for all future drawn objects.
    //! Implicitly calls ne::Camera::getTranslation().
    void setCamera(const ne::Camera &camera);
    //!
    //! Sets the camera for all future drawn objects.
    void setCamera(const ne::Vector2f &camera);

    // Getters
    
    //!
    //! Returns the interpolation value using to interpolate the current frame.
    float getInterpolationValue() const;
    //!
    //! Returns the internal SDL_Window.
    SDL_Window* getWindow();
    //!
    //! Returns the internal SDL_Renderer.
    SDL_Renderer* getRenderer();
    //!
    //! Returns the camera for all drawn objects.
    const ne::Vector2f& getCamera() const;

 private:
    DISALLOW_COPY_AND_ASSIGN(Renderer);

    SDL_Window *window_; 
    SDL_Renderer *render_;
    float interpolation_;

    ne::Vector2f camera_;
};

}
