#pragma once
#include <iostream>
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "Zip.h"
#include "Tools.h"
#include "Copy.h"

namespace ne
{

class Renderer;

//! Loads and draws images.

//!----------------------
//! Overview
//!----------------------
//!
//! Handles the drawing of a basic image from a zip file defined in ne::Engine.
//! As well, also supports interpolation.
class Image
{
 public:
    friend class Renderer;
    Image();
    ~Image();
    explicit Image(const ne::Vector2f &position);
    Image(float x, float y);

    //!
    //! Loads a texture from the zip file defined in ne::Engine at the path <b>file_path</b>.
    void loadZipImage(const std::string &file_path);
    //!
    //! Loads a texture regularly from an on-disk file at path <b>file_path</b>.
    void loadFileImage(const std::string &file_path);
    
    //!
    //! Updates the internal last image positon to the current one. Used for interpolation, and should be called
    //! right before moving the image.
    void updateLast();
    //!
    //! Updates the internal last image position to the specified (<b>x</b>,<b>y</b>) position.
    void updateLast(float x, float y);
    //!
    //! Updates the internal last image position to the vector specified in <b>last_position</b>.
    void updateLast(const ne::Vector2f &last_position);
    //!
    //! Sets the texture sub-rectangle of the image.
    void setTextureRect(const ne::Recti &rect);

    // Setters

    //!
    //! Sets whether or not interpolation will be applied to the image.
    void useInterpolation(bool);
    //!
    //! Sets the texture to be used, instead of loading a new one.
    void setTexture(ne::Image *texture);
    //!
    //! Sets the texture to be used, instead of loading a new one.
    void setTexture(SDL_Texture *texture, int width, int height);
    //!
    //! Sets the image position to the vector.
    //!
    //! If <b>centre</b> is asserted, the position will be set relative to the image's centre.
    void setPosition(const ne::Vector2f &position, bool centre=false);
    //!
    //! Sets the image position to the x and y values.
    //!
    //! If <b>centre</b> is asserted, the position will be set relative to the image's centre.
    void setPosition(float x, float y, bool centre=false);
    //!
    //! Sets the x-coordinate of the image.
    void setX(float);
    //!
    //! Sets the y-coordinate of the image.
    void setY(float);
    //!
    //! Sets the colour mod to affect the current image's texture.
    void setColourMod(const SDL_Color &colour);
    //!
    //! Sets the alpha mod to affect the current image's texture.
    void setAlphaMod(int alpha);
    //!
    //! Sets the rotation of the image (around centre).
    void setRotation(float);
    //!
    //! Sets the scale factor for the image. Scaling is done around an image's centre.
    void setScaling(const ne::Vector2f &scale);
    //!
    //! Controls whether the image is horizontally flipped.
    void setFlip(bool);
    
    // Getters

    //!
    //! Returns whether the image is being interpolated.
    bool isInterpolated() const;
    //!
    //! Returns the SDL_Texture for the image.
    SDL_Texture* getTexture();
    //!
    //! Returns the image's position.
    const ne::Vector2f& getPosition() const;
    //!
    //! Returns the image dimension.
    SDL_Rect* getTargetRect();
    //!
    //! Returns the texture sub-rectangle of the image.
    SDL_Rect* getTextureRect();
    //!
    //! Returns the current x-coordinate of the image.
    float getX() const;
    //!
    //! Returns the current y-coordinate of the image.
    float getY() const;
    //!
    //! Returns the image's width.  This is affected by setTextureRect().
    int getW() const;
    //!
    //! Returns the image's height.  This is affected by setTextureRect().
    int getH() const;
    //!
    //! Returns the original image's width.  This is unaffected by setTextureRect().
    int getImageW() const;
    //!
    //! Returns the original image's height.  This is unaffected by setTextureRect().
    int getImageH() const;
    //!
    //! Returns the colour mod which is affecting the current image's texture.
    const SDL_Color& getColourMod() const;
    //!
    //! Returns the alpha mod which is affecting the current image's texture.
    int getAlphaMod() const;
    //!
    //! Returns the rotation of the image (around centre).
    float getRotation() const;
    //!
    //! Returns the scale factor for the image. Scaling is done around an image's centre.
    const ne::Vector2f& getScaling() const;
    //!
    //! Returns whether the image is horizontally flipped.
    bool getFlip() const;
    //!
    //! Returns the internal last position recorded by updateLastPosition() (or its varieties)
    const ne::Vector2f& getLastPosition() const;
    //!
    //! Returns the calculated interpolation position, given a <b>interpolation</b>.
    ne::Vector2f getInterpolatedPosition(float interpolation) const;
    //!
    //! Returns the file name that was loaded.
    const std::string& getFileName() const;
    
 private:
    DISALLOW_COPY_AND_ASSIGN(Image);
    
    static SDL_Renderer *internal_renderer_;

    SDL_Texture *texture_;
    SDL_Rect target_rect_;
    SDL_Rect texture_rect_;
    SDL_Color colour_;
    int original_width_;
    int original_height_;
    bool flip_;
    bool loaded_texture_;
    std::string file_name_;
    
    bool use_interpolation_;
    ne::Vector2f position_;
    ne::Vector2f last_position_;
    float angle_;
    ne::Vector2f scale_;
    
    void updateTarget();
    void finishLoading(SDL_Surface *surface);
    void deconstruct();

    //!
    //! Sets the internal renderer to be used when translating SDL_Surfaces to SDL_Textures.
    //! Called by ne::Renderer.
    static void setInternalRenderer(SDL_Renderer *renderer);
};

}
