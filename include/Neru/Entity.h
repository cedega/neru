#pragma once
#include "EntityImage.h"
#include "EntityLogic.h"

namespace ne
{

//! Main entity class. ne::EntityImage + ne::EntityLogic.

//!----------------------
//! Overview
//!----------------------
//!
//! The main entity class. Every moveable or collideable object should be an inheritance of ne::Entity.
//!
//! Because ne::Entity is a joint inhertiance of ne::EntityImage and ne::EntityLogic, both public interfaces are available to ne::Entity.
//! Ambiguous functions such as setPosition() have been overloaded in ne::Entity to prevent confusion.
//!
//! ne::Entity has two types of collideable objects to process: Collisions, and Platforms.
//!
//! - 'Collisions' are regular rectangles which the ne::Entity cannot move through.
//! - 'Platforms' are retangles which the ne::Entity can jump through and land on. Any movement in the downwards direction will drop the ne::Entity through the platform.
//!
//! For ne::Entity to properly function, please call in order:
//!
//! - input() sets the velocity, jump flag, and image orientation of the ne::Entity.
//! - logic() moves the ne::Entity based on its velocity and performs movement collisions.
//! - update() sets the ne::Entity's image to the logical location determined by input() and logic().
class Entity : public ne::EntityImage, public ne::EntityLogic
{
 public:
    Entity();
 
    //!
    //! Sets the velocity of the entity in the direction of <b>direction</b>. The magnitude is determined by getSpeed().
    //!
    //! Asserting <b>jump</b> will move the ne::Entity up by getJump() magnitude.
    //!
    //! <b>flip_image</b> determines whether the image will be flipped horizontally when moving left / right.
    //! For example, in platformer games, this argument should be set to true.
    void input(ne::Vector2f direction, bool jump, bool flip_image=false);
    //!
    //! Moves the ne::Entity based on its velocity, and performs collisions on any data set by setCollisions() and setPlatforms().
    //!
    //! <b>interations</b> will determine how many times to incrementally run logic(). This is to increase precision on collisions, at a cost of longer CPU time.
    void logic(const float dt, unsigned int iterations=1);
    //!
    //! Processes animations and positions the ne::EntityImage at the logical location determined by ne::EntityLogic.
    void update(const float dt);
    //!
    //! Must be called and asserted to fall through any platform the ne::Entity is standing on.
    void fallThroughPlatform(bool);
    //!
    //! Untracks any platform the ne::Entity was standing on.
    void clearLastHitPlatform();
    
    // Triggers

    //!
    //! Virtual callback that gets called every time the ne::Entity collides with a Collision or Platform.
    virtual void onCollision(int collision_id);
    //!
    //! Virtual callback that gets called every time the ne::Entity collides with a Collision or Platform from above.
    virtual void onLand();
    
    // Setters

    void setX(float);
    void setY(float); 
    void setPosition(const ne::Vector2f &position);
    void setSpeed(float);
    void setJump(float);
    //!
    //! Sets the collisions to be used in logic(). If left unset, no collisions will be tested.
    void setCollisions(std::vector<ne::Rectf> *collisions);
    //!
    //! Sets the platforms to be used in logic(). If left unset, no platforms will be tested.
    void setPlatforms(std::vector<ne::Rectf> *platforms);

    // Getters
    float getX() const;
    float getY() const;
    const ne::Vector2f getPosition() const;
    float getSpeed() const;
    float getJump() const;
    //!
    //! Returns -1 if the ne::EntityImage is flipped horizontally, and 1 if it's not.
    int getDirection() const;
    //!
    //! Returns the bounding area of the active animation.
    //!
    //! See: ne::EntityData::setActiveFrames(), ne::EntityData::setActiveRegion(), and ne::EntityData::isActive().
    ne::Rectf getActiveRegion() const;
    std::vector<ne::Rectf>* getCollisions();
    std::vector<ne::Rectf>* getPlatforms();
    //!
    //! Returns whether the ne::Entity is jumping. Updated after every input() call.
    bool isJumping() const;
    //!
    //! Returns whether the ne::Entity is moving. Updated after ever input() call.
    bool isMoving() const;
    //!
    //! Returns the index of ne::Entity::getPlatforms() which the ne::Entity is on.
    int getLastHitPlatform() const;
    
 private:
    DISALLOW_COPY_AND_ASSIGN(Entity);
    
    std::vector<ne::Rectf> *collisions_;
    std::vector<ne::Rectf> *platforms_;
    int last_hit_platform_;
    bool fall_through_platform_;
    float speed_;
    float jump_;
    bool jumping_;
    bool moving_;
};

}
