#pragma once
#include "Copy.h"
#include "Tools.h"
#include "Zip.h"
#include <fstream>
#include <iostream>

namespace ne
{

//! Parses Tiled files and loads them into memory.

//!----------------------
//! Overview
//!----------------------
//!
//! Loads layer information from Tiled files so that it can be used in level creation.
class TiledParser
{
 public:
    //!
    //! Creates a 3D array of of layer information of size 4 if max_layers is not specified.
    TiledParser();
    ~TiledParser();

    // Getters
    
    //!
    //! Returns an integer vector with the width of the layers as the x component, and the height of the layers as the y component. 
    const ne::Vector2i& getSize() const;
    //!
    //! Returns an integer vector with the tile width as the x component, and the tile height as the y component. 
    const ne::Vector2i& getTileSize() const;
    //!
    //! Returns the spacing.
    const int getSpacing() const;
    //!
    //! Returns the number of layers.
    int getNumberOfLayers() const;
    //!
    //! Returns the layer information for a given layer.
    int** getTiles(int layer);
 
 protected:
    //!
    //! Reads in the Tiled file and saves it in memory for further manipulation. Writes output to console if file cannot be opened.
    void parseFile(const std::string &file_name);
    //!
    //! Reads in the Tiled file from a zipped folder and saves it in memory for further manipulation.
    void parseZip(const std::string &file_name);
    
 private:
   DISALLOW_COPY_AND_ASSIGN(TiledParser);
   
    //!
    //! Private function. Parses layer information from file into a the 3D array tiles_.
    void parseLayerInfo(const std::string &file_string);
    //!
    //! Private function. Finds tile size and stores it in size_ vector. Finds number of layers and store it in number_of_layers_.
    void findSizeInfo(const std::string &file_string);
    
    ne::Vector2i size_;
    ne::Vector2i tile_size_;
    int spacing_;
    int number_of_layers_;
    int ***tiles_;
    bool tiles_loaded_;
};

}
