#pragma once
#include <vector>
#include "Image.h"

namespace ne
{

//! Extends ne::Image to add texture atlasing.

//!----------------------
//! Overview
//!----------------------
//!
//! Supersedes ne::Image by adding texture atlasing.
//!
//! Texture atlasing allows lots of smaller textures to be compiled into a large texture for performance gain,
//! and then those smaller images can be drawn separately.
//!
//! ne::ImageAtlas does this by storing a vector of rectangles that the user can define.
//! Each defined rectangle is given an index based on the order of creation (first rectangle = index 0),
//! and can be drawn by passing the index to ne::Render::draw().
class ImageAtlas : public ne::Image
{
 public:
    ImageAtlas();

    //!
    //! Creates a rectangle that defines an area of the image.
    //! To draw this, call setTextureRect() with the necessary <b>atlas_id</b>
    //!
    //! Atlas IDs are assigned via order of creation. The first rectangle added will be <b>atlas_id</b> 0, followed by 1, etc.
    void add(const ne::Recti &rect);
    //!
    //! Selects which added rectangle to display. Not that useful, as the actual atlasing is done by ne::Render::draw().
    //! If in doubt, don't use.
	void setTextureRect(unsigned int atlas_id);
    //!
    //! Removes all internal rectangle vector data.
    //! add() will now start at <b>atlas_id</b> 0.
    void clear();
	
	// Getters

    //!
    //! Returns the added rectangle at index <b>atlas_id</b>.
    const ne::Recti& getData(unsigned int atlas_id) const;
    //!
    //! Returns how many rectangles have been added by add().
    unsigned int getSize() const;
	
 private:
    DISALLOW_COPY_AND_ASSIGN(ImageAtlas);

    std::vector<ne::Recti> data_;
};

}
