#pragma once
#include "EntityData.h"
#include "Animation.h"

namespace ne
{

//! Combination of ne::EntityData and ne::Animation.

//!----------------------
//! Overview
//!----------------------
//!
//! Links ne::EntityData and ne::Animation together.
//!
//! While ne::EntityData holds the image's texture, spritesheet, and animation defininitions,
//! ne::Animation is the actor which puts it into motion.
//!
//! ne::EntityImage also supports multiple ne::EntityData for sprites which are split across multiple files.
class EntityImage
{
 public:
    EntityImage();

    //!
    //! Adds a ne::EntityData to the ne::EntityImage.
    void attach(ne::EntityData *animation_set);

    //!
    //! Loads an animation from the currently active ne::EntityImage. 
    //! The <b>load_all</b> flag will load the animation specified from all attached ne::EntityData's.
    //! Leave this option off unless you know what you're doing.
    void loadAnimation(const std::string &animation_name, bool load_all=false);
    //!
    //! Feeds timing into the internal ne::Animation, which increments animation frames.
    //! Also, applies timed animation effects like colour changes.
    void updateAnimations(const float dt);

    //!
    //! Is called by updateAnimations(), and is in charge of changing the frames of an entity.
    virtual void transitionAnimations() = 0;

    //!
    //! Updates the internal "last position" variables to the current position, which contribute to interpolation.
    //! Should call right before moving an entity (ex: ne::Entity::move()).
    void updateLast();
    //!
    //! Updates only the interal X position, which contributes to interpolation.
    void updateLastX();
    //!
    //! Updates only the interal Y position, which contributes to interpolation.
    void updateLastY();
    //!
    //! Applies all displacements and image positions.
    //! Is automatically called by draw(). 
    void updateRenderPosition();

    // Setters

    //!
    //! Sets the x-coordinate of the image.
    void setX(float);
    //!
    //! Sets the y-coordinate of the image.
    void setY(float);
    //!
    //! Sets the position of the image.
    void setPosition(const ne::Vector2f &position);
    //!
    //! Sets the rotation of the image.
    void setRotation(float);
    //!
    //! Sets the scale factor of the image.
    void setScaling(const ne::Vector2f &scaling);
    //!
    //! Controls whether the image will be horiontally flipped.
    void setFlip(bool);
    //!
    //! Sets a base colour modification over the image.
    void setBaseColour(const SDL_Color &colour);
    //!
    //! Changes the colour of the image. Does not affect the base colour.
    void setColour(const SDL_Color &colour);

    // Getters

    //!
    //! Returns the currently selected ne::EntityData.
    ne::EntityData* getActiveData() const;
    //!
    //! Returns the image's position.
    const ne::Vector2f& getPosition() const;
    //!
    //! Returns the internal last image position used for interpolation.
    const ne::Vector2f& getLastPosition() const;
    //!
    //! Returns the image's rotation.
    float getRotation() const;
    //!
    //! Returns the image's scale factor.
    const ne::Vector2f& getScaling() const;
    //!
    //! Returns whether the image is horizontally flipped.
    bool getFlip() const;
    //!
    //! Returns the base colour defined by setBaseColour(). The default is all colour channels 255.
    SDL_Color getBaseColour() const;
    //!
    //! Returns the current colour of the image.
    SDL_Color getColour() const;
    //!
    //! Returns whether an active frame is currently being animated & shown.
    //! Refer to ne::EntityData::setActiveRange().
    bool isActiveFrame() const;
    //!
    //! Returns the rectannelar dimensions defined for an active region.
    //! Refer to ne::EntityData::setActiveRegion().
    ne::Rectf getActiveRegion() const;
    //!
    //! Returns the rectannelar dimensions defined for an active region, given the entity's logical position.
    //! Refer to ne::EntityData::setActiveRegion().
    ne::Rectf getActiveRegion(const ne::Vector2f &position) const;
    //!
    //! Returns whether the animation <b>animation_name</b> is currently being animated & shown.
    bool isAnimating(const std::string &animation_name) const;
    //!
    //! Returns whether the currently running animation has stopped. Will only return true if the animation has looping disabled.
    bool animationStopped() const;
    //!
    //! Returns the name of the current animation.
    const std::string& getLoadedAnimation() const;
    //!
    //! Returns the currently animating frame row, given an animation name.
    int getCurrentRow(const std::string &animation_name);
    //!
    //! Returns the currently animating frame column, given an animation name.
    int getCurrentColumn(const std::string &animation_name);

 protected:
    //! Currently selected ne::EntityData
    ne::EntityData* active_data_;
    ne::Animation animator_;
	
 private:
    DISALLOW_COPY_AND_ASSIGN(EntityImage);

    std::vector<ne::EntityData*> animation_set_;
    std::string loaded_animation_;

    SDL_Color colour_;
    SDL_Color base_colour_;

    ne::Vector2f position_;
    ne::Vector2f last_position_;
    bool flip_;
};

}
