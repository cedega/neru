#pragma once
#include <iostream>
#include <string>
#include "minizip/unzip.h"
#include "Copy.h"

namespace ne
{

//! Data file opening and loading.

//!----------------------
//! Overview
//!----------------------
//!
//! ne::Zip extracts information from the specified Zip File, and loads it into memory for other Neru classes to interpret.
//!
//! Due to the nature of sound files, however, the ne::Zip buffer must be free'd before ne::Zip goes out of scope or gets deleted.
//!
//! As a result, call free(getBuffer()) before deleting or scoping ne::Zip.
class Zip
{
 public:
    Zip();
    explicit Zip(const std::string &file_path);
    
    //!
    //! Loads a file at <b>file_path</b> from the zip.
    //! Refer to the data via getBuffer() and getSize().
    void loadFile(const std::string &file_path);

    // Static
    
    //!
    //! Sets the location of the zip file on disk to use.
    static void setZipPath(const std::string &file_path);
    //!
    //! Sets the password used to encrypt the zip file.
    //! Note that AES isn't support, and only ZipCrypto is.
    static void setZipPassword(const std::string &password);

    // Getters
    
    //!
    //! Returns a pointer refering to the data loaded in memory.
    char* getBuffer();
    //!
    //! Returns the size of the data loaded in memory.
    uLong getSize();
    
 private:
    DISALLOW_COPY_AND_ASSIGN(Zip);

    std::string zip_name_;
    unzFile zip_;
    unz_file_info info_;
    char *buffer_;
    uLong size_;

    static std::string zip_path_;
    static std::string zip_password_;
};

}
