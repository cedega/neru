#pragma once
#include "Tools.h"
#include "Copy.h"

namespace ne
{

//! Screen camera control.

//!----------------------
//! Overview
//!----------------------
//!
//! Handles the offsetting and following of entities and drawn objects.
//! By using ne::Render::setCamera(), this class can keep position data untouched, while still allowing for scrolling of a scene.
class Camera
{
 public:
    //!
    //! Constructs a default camera. Note that you will have to call setScreenSize() and setLevelSize() for the camera to function properly.
    Camera();
    //!
    //! Sets the screen dimensions, and the level dimensions. Screen dimensions can be smaller than the actual screen, to simulate a small playable area within a window.
    Camera(int screen_x, int screen_y, int level_w, int level_h);

    //!
    //! The camera will move towards the target, moving via the getLerpSpeed() amount based on distance.
    //! Must be called every logic step, for the camera to continue moving.
    void moveTo(const ne::Vector2f &target, const float dt);
    //!
    //! Snaps the camera position to the location specified. Serves as a good way of setting the intial position of the camera.
    void snapTo(const ne::Vector2f &target);

    // Setters

    //!
    //! Sets the centre of the camera to the specified position.
    void setPosition(const ne::Vector2f &position);
    //!
    //! Sets the size of the screen. Note that this doesn't have to be the size of the window, but in most cases should be.
    void setScreenSize(const ne::Vector2i &screen_size);
    //!
    //! Sets the size of the playable level.
    void setLevelSize(const ne::Vector2i &level_size);
    //!
    //! Sets how fast the camera will move to the target.
    //! Lower numbers mean the camera will catch up faster.
    //! Do not set to 0, as this will cause a division by 0.  Default value: 8.
    void setLerpSpeed(float lerp_speed);
    //!
    //! Sets an offset for the follow camera.
    //! By default, the camera will place the target in the direct centre of the screen.
    void setOffset(const ne::Vector2f &offset);

    // Getters

    //!
    //! Returns the (x,y) translation that will be applied to all entities being drawn, with interpolation factored.
    ne::Vector2f getTranslation(const float interpolation) const;
    //!
    //! Returns the position of the centre of the camera.
    ne::Vector2f getPosition() const;
    //!
    //! Returns the size of the screen.
    const ne::Vector2i& getScreenSize() const;
    //!
    //! Returns the size of the playable level.
    const ne::Vector2i& getLevelSize() const;
    //!
    //! Returns the lerp speed of the camera.
    //! Lower numbers mean the camera will catch up faster. Default value: 8.
    float getLerpSpeed() const;
    //!
    //! Returns the offset for the follow camera.
    //! By default, the camera will place the target in the direct centre of the screen.
    const ne::Vector2f& getOffset() const;

 private:
    DISALLOW_COPY_AND_ASSIGN(Camera);

    ne::Vector2f position_;
    ne::Vector2f last_position_;
    ne::Vector2f offset_;
    ne::Vector2i level_size_;
    ne::Vector2i screen_size_;
    float lerp_speed_;
    ne::Vector2f target_;
    
    void update(const float dt);
};

}
