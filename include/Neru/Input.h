#pragma once
#include "SDL2/SDL_mouse.h"
#include <SDL2/SDL_keyboard.h>
#include <SDL2/SDL_gamecontroller.h>
#include <iostream>
#include <vector>
#include <map>
#include "Tools.h"

namespace ne
{

typedef const Uint8* Keys;
class Engine;

//! Extended input handling.
    
//!----------------------
//! Overview
//!----------------------
//!
//! Simplifies mouse calls, and supports useful functions like recording a single, new click,
//! or the difference of mouse movement since the last frame.
class Input
{
 public:
    friend class Engine;
    static Input& getInstance()
    {
        static Input instance;
        return instance;
    }
    
    //!
    //! Returns whether the left mouse button is being held down.
    static bool getLeftMouseDown();
    //!
    //! Returns whether the left mouse button is being held down.
    static bool getRightMouseDown();
    //!
    //! Returns whether the left mouse button has been clicked.
    //! Note that, unlike mouseLeftDown(), this function will only be true if the mouse was not previously being clicked.
    //! ie, this will be true <b>only</b> if a new click was made.
    static bool getLeftMouseClick();
    //!
    //! Returns the mouse position relative to the renderer. Note that a ne::Recti is returned to easily be
    //! plugged into ne::intersects(). The width and height of the ne::Recti is set to 1.
    static ne::Recti getMousePosition();
    //!
    //! Returns how much the mouse has moved since the last logic frame (loop).
    static ne::Vector2i getMouseDrag();
    //!
    //! Returns all the keys held. Can be querried as a bool via ne::Keys[SDL_SCANCODE_**KEY**].
    static ne::Keys getKeyboardState();
    //!
    //! Returns all controllers connected.
    static std::vector<SDL_GameController*>* getControllers();
    //!
    //! Returns whether a button on the controller is being held.
    static bool getControllerButton(SDL_GameController *pad, SDL_GameControllerButton button);
    //!
    //! Returns the normalized vector of the left analog stick.
    static ne::Vector2f getControllerLeftAxis(SDL_GameController *pad);
    //!
    //! Returns the normalized vector of the right analog stick.
    static ne::Vector2f getControllerRightAxis(SDL_GameController *pad);

 private:
    Input() {};
    Input(Input const&);
    void operator=(Input const&);
    
    static bool last_mouse_left_down_;
    static ne::Recti last_mouse_position_;
    
    static ne::Vector2f render_ratio_;

    static void setRenderRatio(const ne::Vector2f &render_ratio);
    static void updateData();

    // Game Controllers
    static std::vector<SDL_GameController*> controllers_;
    static std::map<int, SDL_GameController*> controller_mapping_;
    static void initControllers();
    static void addController(int id);
    static void removeController(int id);
};

}
