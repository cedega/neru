#pragma once
#include <iostream>
#include <vector>
#include "Copy.h"

namespace ne
{

//! Processes animations.

//!----------------------
//! Overview
//!----------------------
//!
//! Handles the animation of frames. The animate() function is used over a generic timer to create determinism, which is vital for network games.
class Animation
{
 public:
     Animation();

    //!
    //! Main processing function. Should be called on the logic step, passing in the delta time.
    void animate(const float dt);
    //!
    //! Adds a timing into the internal data. The order of the insertions matter, as the first insertion is the timing for frame #1.
    void insert(int ms);
    //!
    //! Restarts the animaiton at frame #1, with all internal counters reset.
    void reset();
    //!
    //! Removes all internal data, and calls reset().
    void clean();

    // Setters
    void setFrame(unsigned int frame);
    void setTime(int ms);
    void setLoop(bool);
    //!
    //! Forces the animation to abruptly stop.
    void setStop(bool);
    
    // Getters
    unsigned int getFrame() const;
    int getTime() const;
    bool getLoop() const;
    //!
    //! Returns whether the animation has concluded and stopped. Will only be asserted if getLoop() is false.
    bool getStop() const;
    
 private:
    DISALLOW_COPY_AND_ASSIGN(Animation);

    std::vector<int> timing_;
    unsigned int frame_;
    int time_;
    bool loop_;
    bool stop_;
};

}
