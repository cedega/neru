#pragma once
#include "SDL2/SDL.h"
#include "SDL2/SDL_mixer.h"
#include "SDL2/SDL_ttf.h"
#include "SDL2/SDL_net.h"
#include "Input.h"
#include "Renderer.h"
#include "EntityLogic.h"

namespace ne
{

//! Entry point to Neru applications.

//!----------------------
//! Overview
//!----------------------
//!
//! Governs Window creation using SDL, SDL Events, and Gameloop algorithms.
//!
//! Must be inherited.
//!
//!----------------------
//! Memory Management
//!----------------------
//!
//! All Neru Classes (except ne::Zip) manage memory themselves with use of the delete keyword. With ne::Zip, free(getBuffer()) must be called before the data goes out of scope or deleted.
class Engine
{
 public:
    //!
    //! Creates the engine with no zip file specified.
    Engine();
    //!
    //! Creates the engine using the zip file specified at <b>zip_path</b> with ZipCrypto password <b>zip_password</b>.
    Engine(const std::string &zip_path, const std::string &zip_password);
    ~Engine();
    
    //!
    //! Initializes a window with a title and render dimensions.
    //!
    //! <b>linear_scaling</b> determines what texture filtering is applied to images.
    void initWindow(const std::string &window_title, int render_width, int render_height, bool linear_scaling=true);
    
    //!
    //! Virtual callback for the SDL_Events loop.
    //!
    //! This function will be called repetitively for all events in the event queue.
    virtual void events(const SDL_Event &event) = 0;
    //!
    //! The logic() function must be declared in a child of ne::Engine. This function is run at the frequency defined in setPhysicsDt() (default: 1/60).
    virtual void logic(const float dt) = 0;
    //!
    //! The main rendering callback function. Will be called at the rate of the user's vertical sync, or as fast as possible if vertical sync is forced off.
    virtual void render(ne::Renderer *render) = 0;

    //!
    //! The core function of ne::Engine. When this function is run, a gameloop is setup and ran.
    //! After calling this function, the user must define the features of his/her game in render() and logic().
    void runGameLoop();
    //!
    //! Cancels the gameloop being ran in runGameLoop().
    //! Note that this will wait for the current loop iteration to be done before ending.
    void kill();
    //!
    //! Resets the accumulator defined in runGameLoop(). Note that this "accumulator" refers to the accumulation of logic time used in a semi-fixed timestep.
    void resetAccumulator();

    // Static
    
    //!
    //! Sets the frequency execution of the logic() function.
    static void setPhysicsDt(float dt);
    //!
    //! Returns the frequency execution of the logic() function.
    static float getPhysicsDt();

    // Setters
   
    //!
    //! If <b>time_ms</b> is a value above 0, the gameloop will pause <b>time_ms</b> milliseconds after every iteration.
    //! This is useful for throttling down CPU usage when the game isn't active.
    void setLoopDelay(int time_ms);
    //!
    //! Enables or disables fullscreening the window.
    //!
    //! <b>fake</b> determines if actual fullscreen of "fake" fullscreen is used.
    //! Fake fullscreen means the window is the same as the screen, but no videomode changes will be made.
    void setFullScreen(bool on, bool fake=false);
    //!
    //! Sets the interal semi-fixed timestep "accumulator" used in the gameloop.
    //! see: resetAccumulator()
    void setLogicAccumulator(float f);
    //!
    //! Sets the randomization seed to be used for ne::random() functions inside ne::Tools.
    void setSeed(unsigned int seed);

    //!
    //! Sets the clear colour of the window.
    void setClearColour(int r, int g, int b);
    //!
    //! Sets the text title of the window.
    void setWindowTitle(const std::string &title);
    //!
    //! Loads a window icon from the zip file.
    void setWindowIcon(const std::string &path_in_zip);

    // Getters
    
    //!
    //! Returns the delay being introduced by setLoopDelay().
    int getLoopDelay() const;
    //!
    //! Returns whether the window is in fullscreen mode.
    bool getFullScreen() const;
    //!
    //! Returns the progress of the logical semi-fixed timestep "accumulator."
    //! see: setLogicAccumulator(), resetAccumulator().
    float getLogicAccumulator() const;
    //!
    //! Returns the ne::random() randomization seed defined in setSeed().
    unsigned int getSeed() const;
    //!
    //! Returns the logical size of the renderer.
    const ne::Vector2i& getRenderSize() const;
    //!
    //! Returns the window size. Note that on some devices, this is different from render size.
    //! (See: getRenderSize())
    ne::Vector2i getWindowSize() const;
    //!
    //! Returns the ratio between the renderer and the screen size.  Screen:Renderer
    //! So if the screen is 2x bigger, the ratio returned will be 2.
    ne::Vector2f getRenderRatio() const;
    //!
    //! Returns the inverse of the last frame time.
    float getFPS() const;
    
 protected:
    ne::Renderer renderer_;

 private:
    DISALLOW_COPY_AND_ASSIGN(Engine);

    // Engine Data
    bool alive_;
    float fps_;
    float accumulator_;
    bool reset_accumulator_;
    static float dt_;
    int loop_delay_time_;
    unsigned int seed_;
    bool fullscreen_;

    // Window & Events
    SDL_Window *window_;
    SDL_Color clear_colour_;

    // Window Data
    std::string window_title_;
    ne::Vector2i render_size_;
    
    void init();
    void delay();
};

}
