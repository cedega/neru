#include "Neru/Music.h"

namespace ne
{

Music::Music()
{
    zip_used_ = false;
    music_ = NULL;
    zip_rw_ = NULL;
    zip_music_buffer_ = NULL;
    volume_ = MIX_MAX_VOLUME;
}

Music::~Music()
{
    deconstruct();
}

void Music::deconstruct()
{
    Mix_HaltMusic();
    Mix_FreeMusic(music_);
    if (zip_used_)
    {
        free(zip_music_buffer_);
        SDL_RWclose(zip_rw_);
        zip_used_ = false;
    }
}

void Music::loadZipMusic(const std::string &file_path)
{
    deconstruct();

    ne::Zip zip(file_path);
    zip_music_buffer_ = zip.getBuffer();
    
    zip_rw_ = SDL_RWFromMem(zip.getBuffer(),zip.getSize());
    music_ = Mix_LoadMUS_RW(zip_rw_,0);
    
    if (music_ == NULL)
        std::cout << "Error loading \"" << file_path << "\" from ZIP\n";
    
    file_name_ = file_path.substr(file_path.find_last_of("/") + 1);
    zip_used_ = true;
}

void Music::loadFileMusic(const std::string &file_path)
{
    deconstruct();
    
    music_ = Mix_LoadMUS(file_path.c_str());
    
    if (music_ == NULL)
        std::cout << "Error loading \"" << file_path << "\" from file\n";
    

    file_name_ = file_path.substr(file_path.find_last_of("/") + 1);
    zip_used_ = false;
}

void Music::play(int loop)
{
    if (Mix_PlayMusic(music_, loop) != 0)
        std::cout << Mix_GetError() << "\n";
}

void Music::stop()
{
    Mix_HaltMusic();
}

void Music::fadeOut(int ms)
{
    Mix_FadeOutMusic(ms);
}

void Music::fadeIn(int ms, int loops)
{
    Mix_FadeInMusic(music_, loops, ms);
}

// Static

bool Music::playing()
{
    return Mix_PlayingMusic() == 1;
}

// Setters

void Music::setVolume(int volume)
{
    if (volume > MIX_MAX_VOLUME) volume = MIX_MAX_VOLUME;
    else if (volume < 0) volume = 0;
    
    volume_ = volume;
    Mix_VolumeMusic(volume_);
}

// Getters

int Music::getVolume() const
{
    return volume_;
}

const std::string& Music::getFileName() const
{
    return file_name_;
}

}
