#include "Neru/Entity.h"

namespace ne
{

Entity::Entity()
{
    collisions_ = NULL;
    platforms_ = NULL;
    last_hit_platform_ = -1;
    fall_through_platform_ = false;
    speed_ = 0.0f;
    jump_ = 0.0f;
    jumping_ = false;
    moving_ = false;
}

void Entity::input(ne::Vector2f direction, bool jump, bool flip_image)
{
    direction.normalize();
    moving_ = ne::absval(direction.x) > 0.0f;

    if (flip_image && moving_)
    {
        if (direction.x > 0)
            ne::EntityImage::setFlip(false);
        else
            ne::EntityImage::setFlip(true);
    }

    ne::Vector2f velocity;
    velocity.x = direction.x * speed_;
    velocity.y = direction.y * speed_;

    if (jump && !jumping_)
    {
        setVelocity(ne::Vector2f(getVelocity().x, 0.0f));
        velocity.y -= jump_;
        jumping_ = true;
        last_hit_platform_ = -1;
    }

    // Only append Y velocity if gravity exists
    if (ne::absval(getGravity()) > 0.0f)
    {
        setVelocity(ne::Vector2f(velocity.x, getVelocity().y + velocity.y));
    }
    else
    {
        setVelocity(velocity);
    }
}

void Entity::logic(const float dt, unsigned int iterations)
{
    ne::EntityImage::updateLast();
    const float step = dt / iterations;
    
    for (unsigned int i = 0; i < iterations; i++)
    {
        ne::EntityLogic::move(step);
        
        // Collisions
        if (collisions_ != NULL)
        {
            ne::Rectf p;

            for (unsigned int j = 0; j < collisions_->size(); j++)
            {
                p = getHitbox();
                int col = ne::EntityLogic::collide(&p,&collisions_->at(j));
                if (col > 0)
                {
                    ne::EntityLogic::setPosition(ne::Vector2f(p.x, p.y));
                    onCollision(col);
                }
            }
        }
        
        // Platforms
        if (platforms_ != NULL)
        {
            for (unsigned int j = 0; j < platforms_->size(); j++)
            {
                if (ne::intersects(getHitbox(), platforms_->at(j)))
                {
                    // If the entity is not falling through the platform he's on
                    if (!fall_through_platform_ || static_cast<int>(j) != last_hit_platform_)
                    {
                        // If the entity is moving downwards and the centre of the entity is above the platform
                        if (getVelocity().y > 0 && getCentre().y < platforms_->at(j).y)
                        {
                            // The 0.01f is because that's the "bump" factor for ne::EntityLogic's collide()
                            setY(platforms_->at(j).y - getHitbox().h - 0.01f);

                            // Remove all Y velocity
                            setVelocity(ne::Vector2f(getVelocity().x, 0));
                            // Set record the last platform the entity has "hit" (or landed on)
                            last_hit_platform_ = j;
                            // Call onLand() trigger normally done by ne::EntityLogic::collide()
                            onLand();
                            onCollision(-1);

                            fall_through_platform_ = false;
                        }
                    }
                }
            }
        }
    }
}

void Entity::update(const float dt)
{
    // Apply animations
    updateAnimations(dt);
    
    ne::EntityImage::setX(ne::EntityLogic::getX());
    ne::EntityImage::setY(ne::EntityLogic::getY());
}

void Entity::fallThroughPlatform(bool b)
{
    fall_through_platform_ = b;
}

void Entity::clearLastHitPlatform()
{
    last_hit_platform_ = -1;
}

// Triggers

void Entity::onCollision(int collision_id)
{

}

void Entity::onLand()
{
    jumping_ = false;
    fallThroughPlatform(false);
}

// Setters

void Entity::setX(float x)
{
    ne::EntityLogic::setX(x);
    ne::EntityImage::setX(ne::EntityLogic::getX());
    ne::EntityImage::updateLast();
}

void Entity::setY(float y) 
{
    ne::EntityLogic::setY(y);
    ne::EntityImage::setY(ne::EntityLogic::getY());
    ne::EntityImage::updateLast();
}

void Entity::setPosition(const ne::Vector2f &position)
{
    ne::EntityLogic::setPosition(position);
    ne::EntityImage::setX(ne::EntityLogic::getX());
    ne::EntityImage::setY(ne::EntityLogic::getY());
    ne::EntityImage::updateLast();
}

void Entity::setSpeed(float s)
{
    speed_ = s;
}

void Entity::setJump(float j)
{
    jump_ = j;
}

void Entity::setCollisions(std::vector<ne::Rectf> *collisions)
{
    collisions_ = collisions;
}

void Entity::setPlatforms(std::vector<ne::Rectf> *platforms)
{
    platforms_ = platforms;
}

// Getters

float Entity::getX() const
{
    return ne::EntityLogic::getX();
}

float Entity::getY() const
{
    return ne::EntityLogic::getY();
}

const ne::Vector2f Entity::getPosition() const
{
    return ne::EntityLogic::getPosition();
}

float Entity::getSpeed() const
{
    return speed_;
}

float Entity::getJump() const
{
    return jump_;
}

int Entity::getDirection() const
{
    if (ne::EntityImage::getFlip())
        return -1;
    else
        return 1;
}

ne::Rectf Entity::getActiveRegion() const
{
    return ne::EntityImage::getActiveRegion(ne::EntityLogic::getPosition());
}

std::vector<ne::Rectf>* Entity::getCollisions()
{
    return collisions_;
}

std::vector<ne::Rectf>* Entity::getPlatforms()
{
    return platforms_;
}

bool Entity::isJumping() const
{
    return jumping_;
}

bool Entity::isMoving() const
{
    return moving_;
}

int Entity::getLastHitPlatform() const
{
    return last_hit_platform_;
}

}
