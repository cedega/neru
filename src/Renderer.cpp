#include "Neru/Renderer.h"

namespace ne
{

Renderer::Renderer()
{
    window_ = NULL;
    render_ = NULL;
    interpolation_ = 1.0f;
    camera_.x = 0;
    camera_.y = 0;
}

bool Renderer::bindWindow(SDL_Window *window)
{
    window_ = window;
    render_ = SDL_CreateRenderer(window_, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    
    ne::Image::setInternalRenderer(render_);
    ne::Text::setInternalRenderer(render_);
    
    return window_ != NULL && render_ != NULL;
}

void Renderer::draw(ne::Image &image)
{
    ne::Vector2f original = image.getPosition();
    
    // Apply Interpolation
    if (image.isInterpolated())
        image.setPosition(image.getInterpolatedPosition(interpolation_));

    image.setPosition(image.getPosition() - camera_);
    
    // Apply Flip
    SDL_RendererFlip flip = SDL_FLIP_NONE;

    if (image.getFlip())
        flip = SDL_FLIP_HORIZONTAL;
    
    // Apply Scaling around centre
    SDL_Rect target = *image.getTargetRect();

    target.w *= image.getScaling().x;
    target.h *= image.getScaling().y;
    int difference_x = target.w - image.getTargetRect()->w;
    int difference_y = target.h - image.getTargetRect()->h;
    target.x -= difference_x/2;
    target.y -= difference_y/2;

    SDL_RenderCopyEx(render_, image.getTexture(), image.getTextureRect(), &target, image.getRotation(), NULL, flip);

    image.setPosition(original);
}

void Renderer::draw(ne::ImageAtlas &atlas, int atlas_id)
{
    atlas.setTextureRect(atlas_id);
    draw(reinterpret_cast<ne::Image&>(atlas));
}

void Renderer::draw(ne::Text &text)
{
    if (text.getString().empty())
        return;

    ne::Vector2f original = text.getPosition();
    
    if (text.isInterpolated())
        text.setPosition(text.getInterpolatedPosition(interpolation_));

    text.setPosition(text.getPosition() - camera_);

    // Apply Scaling around centre
    SDL_Rect target = *text.getTargetRect();

    target.w *= text.getScaling().x;
    target.h *= text.getScaling().y;
    int difference_x = target.w - text.getTargetRect()->w;
    int difference_y = target.h - text.getTargetRect()->h;
    target.x -= difference_x/2;
    target.y -= difference_y/2;

    SDL_RenderCopyEx(render_, text.getTexture(), text.getTextureRect(), &target, 0, NULL, SDL_FLIP_NONE);
    
    text.setPosition(original);
}

void Renderer::draw(ne::EntityImage &entity_image)
{
    entity_image.updateRenderPosition();
    draw(*entity_image.getActiveData()); 
}

void Renderer::draw(ne::TileMap &tile_map, int layer)
{
    SDL_Rect target;
    target.w = tile_map.getTileSize().x;
    target.h = tile_map.getTileSize().y;

    int window_width, window_height;
    SDL_GetWindowSize(window_, &window_width, &window_height);

    int start_x = (camera_.x / target.w) - 1;
    int start_y = (camera_.y / target.h) - 1;
    if (start_x < 0)
        start_x = 0;
    if (start_y < 0)
        start_y = 0;
   
    const float restart_pos = -camera_.x + (start_x * target.w);
    const int num_tiles_x = (window_width / target.w) + 2;
    const int num_tiles_y = (window_height / target.h) + 2;
    target.x = restart_pos;
    target.y = -camera_.y + (start_y * target.h);

    for (int y = start_y; y < start_y + num_tiles_y; y++)
    {
        for (int x = start_x; x < start_x + num_tiles_x; x++)
        {
            int index = tile_map.getTiles(layer)[y][x];
            if (index >= 0)
            {
                tile_map.setRenderTile(index);
                SDL_RenderCopy(render_, tile_map.getTexture(), tile_map.getTextureRect(), &target);
            }
            target.x += tile_map.getTileSize().x;
        }
        target.x = restart_pos;
        target.y += tile_map.getTileSize().y;
    }
}

void Renderer::draw(ne::Rectf rect, const SDL_Color &colour)
{
    // Set colour of lines to black
    SDL_SetRenderDrawColor(render_, colour.r, colour.g, colour.b, colour.a);

    // Apply Camera
    rect.x -= camera_.x;
    rect.y -= camera_.y;
    
    // Top line
    SDL_RenderDrawLine(render_, rect.x, rect.y, rect.x + rect.w, rect.y);
    // Left line
    SDL_RenderDrawLine(render_, rect.x, rect.y,rect.x, rect.y + rect.h);
    // Right line
    SDL_RenderDrawLine(render_, rect.x + rect.w, rect.y, rect.x + rect.w, rect.y + rect.h);
    // Bottom line
    SDL_RenderDrawLine(render_, rect.x, rect.y + rect.h, rect.x + rect.w, rect.y + rect.h);
}

void Renderer::drawLine(const ne::Vector2f &point1, const ne::Vector2f &point2, const SDL_Color &colour)
{
    SDL_SetRenderDrawColor(render_, colour.r, colour.g, colour.b, colour.a);
    // Draw line, with Camera applied
    SDL_RenderDrawLine(render_, point1.x - camera_.x, point1.y - camera_.y, point2.x - camera_.x, point2.y - camera_.y);
}

// Setters

void Renderer::setInterpolationValue(float interpolation)
{
    interpolation_ = interpolation; 
}

void Renderer::setCamera(const ne::Camera &camera)
{
    camera_ = camera.getTranslation(interpolation_);
}

void Renderer::setCamera(const ne::Vector2f &camera)
{
    camera_ = camera;
}

// Getters

float Renderer::getInterpolationValue() const
{
    return interpolation_;
}

SDL_Window* Renderer::getWindow()
{
    return window_;
}

SDL_Renderer* Renderer::getRenderer()
{
    return render_;
}

const ne::Vector2f& Renderer::getCamera() const
{
    return camera_;
}

}
