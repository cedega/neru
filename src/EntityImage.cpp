#include "Neru/EntityImage.h"

namespace ne
{

EntityImage::EntityImage()
{
    flip_ = false;
    active_data_ = NULL;
    base_colour_ = ne::toSDLColor(255,255,255);
    colour_ = base_colour_;
}

void EntityImage::attach(ne::EntityData *animation)
{
    if (!active_data_)
        active_data_ = animation;

    animation_set_.push_back(animation);
}

void EntityImage::loadAnimation(const std::string &animation_name, bool load_all)
{
    // If animation trying to load is not found in the current data
    if (!active_data_->animationExists(animation_name))
    {
        // Search other data for a match.  If true, set the active_data_ pointer to that
        for (unsigned int i = 0; i < animation_set_.size(); i++)
        {
            if (animation_set_[i]->animationExists(animation_name))
            {
                active_data_ = animation_set_[i];

                // Force the next animation load
                loaded_animation_.clear();

                break;
            }
        }
    }

    // Ignore the load if the animation names are the same
    if (animation_name.compare(loaded_animation_) == 0)
        return;

    // Load Animation
    
    animator_.clean();

    for (unsigned int i = 0; i < active_data_->getNumberOfFrames(animation_name); i++)
    {
        animator_.insert(active_data_->getFrameTiming(animation_name,i+1)); 
    }

    // Loads the animation into every single animation set
    if (load_all)
    {
        for (unsigned int i = 0; i < animation_set_.size(); i++)
        {
            const int row = animation_set_[i]->convertToRow(animation_set_[i]->getFrameRange(animation_name).x);
            const int col = animation_set_[i]->convertToColumn(animation_set_[i]->getFrameRange(animation_name).x);

            animation_set_[i]->setSprite(row,col);
        }
    }
    // Only loads for the active animation
    else
    {
        const int row = active_data_->convertToRow(active_data_->getFrameRange(animation_name).x);
        const int col = active_data_->convertToColumn(active_data_->getFrameRange(animation_name).x);

        active_data_->setSprite(row,col);
    }

    loaded_animation_ = animation_name;

    animator_.setLoop(active_data_->getLoop(animation_name));
}

void EntityImage::updateAnimations(const float dt)
{
    animator_.animate(dt);

    // Overloaded by child objects to set animation transitions
    transitionAnimations();

    // Get frame from normalized animator data
    const int frame = active_data_->getFrameRange(loaded_animation_).x + animator_.getFrame() - 1;
    const int row = active_data_->convertToRow(frame);
    const int col = active_data_->convertToColumn(frame);

    active_data_->setSprite(row,col);

    // Set colours
    for (unsigned int i = 0; i < animation_set_.size(); i++)
    {
        animation_set_[i]->setColourMod(colour_);
    }
}

void EntityImage::updateLast()
{
    last_position_ = position_;
}

void EntityImage::updateLastX()
{
    last_position_.x = position_.x;
}

void EntityImage::updateLastY()
{
    last_position_.y = position_.y;
}

void EntityImage::updateRenderPosition()
{
    const int frame_dx = active_data_->getFrameDisplacement(loaded_animation_,animator_.getFrame()).x;
    const int frame_dxf = -1*frame_dx;
    const int frame_dy = active_data_->getFrameDisplacement(loaded_animation_,animator_.getFrame()).y;

    const ne::Vector3i disp = active_data_->getBaseDisplacement();

    const float x = position_.x + disp.x + frame_dx;
    const float xf = position_.x + disp.z + frame_dxf;
    const float y = position_.y + disp.y + frame_dy;

    active_data_->setFlip(flip_);
    
    if(!flip_)
    {
        active_data_->updateLast(last_position_.x + disp.x + frame_dx, last_position_.y + disp.y + frame_dy);
        active_data_->setPosition(x,y);
    }
    else
    {
        active_data_->updateLast(last_position_.x + disp.z + frame_dxf, last_position_.y + disp.y + frame_dy);
        active_data_->setPosition(xf,y);
    }
}

// Setters

void EntityImage::setX(float x)
{
    position_ = ne::Vector2f(x,position_.y);
}

void EntityImage::setY(float y)
{
    position_ = ne::Vector2f(position_.x,y);
}

void EntityImage::setPosition(const ne::Vector2f &position)
{
    position_ = position;
}

void EntityImage::setRotation(float rotation)
{
    active_data_->setRotation(rotation);
}

void EntityImage::setScaling(const ne::Vector2f &scaling)
{
    active_data_->setScaling(scaling);
}

void EntityImage::setFlip(bool b) 
{
    flip_ = b;
}

void EntityImage::setBaseColour(const SDL_Color &colour)
{
    base_colour_ = colour;
}

void EntityImage::setColour(const SDL_Color &colour)
{
    colour_ = colour;
}

// Getters

ne::EntityData* EntityImage::getActiveData() const
{
    return active_data_;
}

const ne::Vector2f& EntityImage::getPosition() const
{
    return position_;
}

const ne::Vector2f& EntityImage::getLastPosition() const
{
    return last_position_;
}

float EntityImage::getRotation() const
{
    return active_data_->getRotation();
}

const ne::Vector2f& EntityImage::getScaling() const
{
    return active_data_->getScaling();
}

bool EntityImage::getFlip() const
{
    return flip_;
}

SDL_Color EntityImage::getBaseColour() const
{
    return base_colour_;
}

SDL_Color EntityImage::getColour() const
{
    return colour_;
}

bool EntityImage::isActiveFrame() const
{
    return active_data_->isActive(loaded_animation_,animator_.getFrame());
}

ne::Rectf EntityImage::getActiveRegion() const
{
    return active_data_->getActiveRegion(loaded_animation_,animator_.getFrame());
}

ne::Rectf EntityImage::getActiveRegion(const ne::Vector2f &position) const
{
    ne::Vector3i disp = active_data_->getBaseDisplacement();
    ne::Vector2i f_disp = active_data_->getFrameDisplacement(loaded_animation_,animator_.getFrame());
    ne::Rectf ret = getActiveRegion();
    
    //Determines the flipped location of the hitbox
    float flip_loc = active_data_->getSpriteBoxWidth() - ret.x - ret.w + 1;
    
    ret.x = flip_ ? flip_loc + disp.z - f_disp.x + position.x : ret.x + disp.x + f_disp.x + position.x;
    ret.y = ret.y + disp.y + f_disp.y + position.y;
    
    return ret;
}

bool EntityImage::isAnimating(const std::string& animation_name) const
{
    return loaded_animation_.compare(animation_name) == 0;
}

bool EntityImage::animationStopped() const
{
    return animator_.getStop();
}

const std::string& EntityImage::getLoadedAnimation() const
{
    return loaded_animation_;
}

int EntityImage::getCurrentRow(const std::string &animation_name)
{
    int frame = active_data_->getFrameRange(animation_name).x + animator_.getFrame() - 1;
    return active_data_->convertToRow(frame);
}

int EntityImage::getCurrentColumn(const std::string &animation_name)
{
    int frame = active_data_->getFrameRange(animation_name).x + animator_.getFrame() - 1;
    return active_data_->convertToColumn(frame);
}

}
