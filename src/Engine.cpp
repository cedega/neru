#include "Neru/Engine.h"

namespace ne
{

float Engine::dt_;

Engine::Engine()
{
    init();
}

Engine::Engine(const std::string &zip_path, const std::string &zip_password)
{
    init();
	ne::Zip::setZipPath(zip_path);
    ne::Zip::setZipPassword(zip_password);
}

Engine::~Engine()
{
    SDL_DestroyRenderer(renderer_.getRenderer());
	SDL_DestroyWindow(window_);
    Mix_CloseAudio();
    TTF_Quit();
    SDLNet_Quit();
    SDL_Quit();
}

void Engine::init()
{
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
        std::cout << "SDL Error: " << SDL_GetError() << "\n";
    
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) != 0)
        std::cout << "Failed to initialize SDL_mixer\n";
    
    if (TTF_Init() != 0)
        std::cout << "Failed to initialize SDL_ttf\n";

    if (SDLNet_Init() != 0)
        std::cout << "Failed to initialize SDL_net\n";
	
    alive_ = true;
    reset_accumulator_ = false;
    fullscreen_ = false;

    window_title_ = "GUMI";

    accumulator_ = 0.0;
    fps_ = 0;
    dt_ = 1.0f / 60.0f;
    ne::EntityLogic::setPhysicsDt(dt_);
    loop_delay_time_ = 0;

    setSeed(time(NULL));

    SDL_Color temp;
    temp.r = 100;
    temp.g = 100;
    temp.b = 100;
    temp.a = 255;
    clear_colour_ = temp;

    ne::Input::initControllers();
}

void Engine::initWindow(const std::string &window_title, int render_width, int render_height, bool linear_scaling)
{
    window_title_ = window_title;
    render_size_.x = render_width;
    render_size_.y = render_height;
	
    window_ = SDL_CreateWindow(window_title_.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, render_width, render_height, SDL_WINDOW_SHOWN);
    
    renderer_.bindWindow(window_);
    SDL_RenderSetLogicalSize(renderer_.getRenderer(), render_width, render_height);
    
    ne::Input::setRenderRatio(getRenderRatio());
    
    if (linear_scaling)
        SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
    else
        SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest");
}

void Engine::runGameLoop()
{
    Uint32 current_time = SDL_GetTicks();
    Uint32 new_time;
    float frame_time;
    SDL_Event event;

    while (alive_)
    {
        new_time = SDL_GetTicks();;
        frame_time = (new_time - current_time) / 1000.0f;
        current_time = new_time;

        // Prevent Spiral of Death
        if (frame_time > 0.25)
        {
            frame_time = 0.25;
        }

        accumulator_ += frame_time;

        if (reset_accumulator_)
        {
            accumulator_ = 0;
            reset_accumulator_ = false;
        }
        
        // Set FPS
        fps_ = 1.0f/frame_time;
        
        // Run Logic at dt frequency
        while (accumulator_ >= dt_)
        {
            while (SDL_PollEvent(&event))
            {
                switch (event.type)
                {
                case SDL_CONTROLLERDEVICEADDED: ne::Input::addController(event.cdevice.which); break;
                case SDL_CONTROLLERDEVICEREMOVED: ne::Input::removeController(event.cdevice.which); break;
                default: events(event);
                }
            }

            logic(dt_);
            accumulator_ -= dt_;

            ne::Input::updateData();
        }

        renderer_.setInterpolationValue(accumulator_ / dt_);
        
        SDL_SetRenderDrawColor(renderer_.getRenderer(),clear_colour_.r,clear_colour_.g,clear_colour_.b,clear_colour_.a);
        SDL_RenderClear(renderer_.getRenderer());

        render(&renderer_);
        
        SDL_RenderPresent(renderer_.getRenderer());
        
        delay();
    }
}

void Engine::delay()
{
    if (loop_delay_time_ > 0)
    {
        SDL_Delay(loop_delay_time_);
    }
}

void Engine::kill()
{
    alive_ = false;
}

void Engine::resetAccumulator()
{
    reset_accumulator_ = true; 
}

// Static

void Engine::setPhysicsDt(float dt)
{
    dt_ = dt;
    ne::EntityLogic::setPhysicsDt(dt);
}

float Engine::getPhysicsDt()
{
    return dt_;
}

// Setters

void Engine::setLoopDelay(int time_ms)
{
    loop_delay_time_ = time_ms;
}

void Engine::setFullScreen(bool on, bool fake)
{
    fullscreen_ = on;
    
    if (fullscreen_)
    {
        if (fake)
            SDL_SetWindowFullscreen(window_, SDL_WINDOW_FULLSCREEN_DESKTOP);
        else
            SDL_SetWindowFullscreen(window_, SDL_WINDOW_FULLSCREEN);
    }
    else
        SDL_SetWindowFullscreen(window_, 0);
}

void Engine::setLogicAccumulator(float f)
{
    accumulator_ = f;
}

void Engine::setSeed(unsigned int seed)
{
    seed_ = seed;
    std::srand(seed);
}

void Engine::setClearColour(int r, int g, int b)
{
	SDL_Color temp;
	temp.r = r;
	temp.g = g;
	temp.b = b;
    clear_colour_ = temp;
}

void Engine::setWindowTitle(const std::string &title)
{
    SDL_SetWindowTitle(window_,title.c_str());
}

void Engine::setWindowIcon(const std::string &path_in_zip)
{
    ne::Zip zip(path_in_zip);
    
    SDL_RWops *rw = SDL_RWFromMem(zip.getBuffer(), zip.getSize());
    SDL_Surface *surface = IMG_Load_RW(rw, 0);
    
    SDL_SetWindowIcon(window_, surface);
    
    SDL_FreeSurface(surface);
    free(zip.getBuffer());
    SDL_RWclose(rw);
}

// Getters

int Engine::getLoopDelay() const
{
    return loop_delay_time_;
}

bool Engine::getFullScreen() const
{
    return fullscreen_;
}

float Engine::getLogicAccumulator() const
{
    return accumulator_;
}

unsigned int Engine::getSeed() const
{
    return seed_;
}

const ne::Vector2i& Engine::getRenderSize() const
{
    return render_size_;
}
    
ne::Vector2i Engine::getWindowSize() const
{
    ne::Vector2i size;
    SDL_GetWindowSize(window_,&size.x,&size.y);
    return size;
}
    
ne::Vector2f Engine::getRenderRatio() const
{
    ne::Vector2f ratio;
    ne::Vector2i window_size = getWindowSize();
    ratio.x = static_cast<float>(window_size.x)/render_size_.x;
    ratio.y = static_cast<float>(window_size.y)/render_size_.y;
    return ratio;
}
    
float Engine::getFPS() const
{
    return fps_;
}

}
