#include "Neru/TiledParser.h"

namespace ne
{

TiledParser::TiledParser()
{
    number_of_layers_ = 0;
    tiles_loaded_ = false;
}

TiledParser::~TiledParser()
{
    // De-construct tiles_.
    if (tiles_loaded_)
    {
        for (int i = 0; i < number_of_layers_; i++)
        {
            for (int j = 0; j < size_.y; j++)
                delete[] tiles_[i][j];
            
            delete[] tiles_[i];
        }
        
        delete[] tiles_;
    }
}

void TiledParser::parseFile(const std::string &file_name)
{
    std::string file_string = "";

    // Open file and save it in string file_string.
    if (ne::fileExists(file_name))
    {
        std::ifstream file(file_name.c_str());
        std::string line;
        
        while (std::getline(file, line))
            file_string += line;
    }
    else
        std::cout << "'" << file_name.c_str() << "' could not be opened. Please make sure the file exists." << std::endl;
        
    // Finds the size of each tiles and the number of layers.
    findSizeInfo(file_string);
    // Parses each layer's data and loads it into tiles_.
    parseLayerInfo(file_string); 
}

void TiledParser::parseZip(const std::string &file_name)
{
    ne::Zip zip(file_name.c_str());
    std::string file_string(zip.getBuffer());
    free(zip.getBuffer());
    
    // Finds the size of each tiles and the number of layers.
    findSizeInfo(file_string);
    // Parses each layer's data and loads it into tiles_.
    parseLayerInfo(file_string); 
}

void TiledParser::findSizeInfo(const std::string &file_string)
{
    // Stores width of layers in size_.x.
    std::string size = "";
    int index = file_string.find(" width=") + 8;
    
    while (file_string.at(index) != '"')
    {
        size += file_string.at(index);
        index++;
    }

    size_.x = atoi(size.c_str());
    
    // Stores height of layers in size_.y.
    index = file_string.find(" height=") + 9;
    size = "";
    
    while (file_string.at(index) != '"')
    {
        size += file_string.at(index);
        index++;
    }

    size_.y = atoi(size.c_str());
    
    // Stores tilewidth in tile_size_.x.
    index = file_string.find(" tilewidth=") + 12;
    size = "";
    
    while (file_string.at(index) != '"')
    {
        size += file_string.at(index);
        index++;
    }

    tile_size_.x = atoi(size.c_str());
    
    // Stores tileheight in tile_size_.y.
    index = file_string.find(" tileheight=") + 13;
    size = "";
    
    while (file_string.at(index) != '"')
    {
        size += file_string.at(index);
        index++;
    }

    tile_size_.y = atoi(size.c_str());
  
    // Stores spacing in spacing_.
    index = file_string.find(" spacing=") + 10;
    size = "";
    
    while (file_string.at(index) != '"')
    {
        size += file_string.at(index);
        index++;
    }

    spacing_ = atoi(size.c_str());
  
    // Finds the number of layers and saves it in number_of_layers_.
    index = 0;
    
    while (file_string.find("<layer name=", index) != std::string::npos)
    {
        number_of_layers_++;
        index += file_string.find("<layer name=", index) + 1;
    }
    
    tiles_ = new int**[number_of_layers_];
}

void TiledParser::parseLayerInfo(const std::string &file_string)
{
    // Finish initializing tiles_.
    for (int i = 0; i < number_of_layers_; i++)
    {
        tiles_[i] = new int*[size_.y];
        
        for (int j = 0; j < size_.y; j++)
            tiles_[i][j] = new int[size_.x];
    }
    
    // Parse all layer data and put it into tiles_.
    int index = 0;
    for (int i = 0; i < number_of_layers_; i++)
   {
        index = file_string.find("<data encoding=\"csv\">", index) + 21;
 
        while (file_string.at(index) == ' ')
            index++;
        
        std::string value;
        for (int j = 0; j < size_.y; j++)
        {    
            for (int k = 0; k < size_.x; k++)
            {
                value = "";
                while (file_string.at(index) != ',' && file_string.at(index) != ' '  && file_string.at(index) != '<')
                {
                    value += file_string.at(index);
                    index++;
                }
                index++;
                tiles_[i][j][k] = atoi(value.c_str()) - 1;
            }
        }
    }
    
    tiles_loaded_ = true;
}

// Getters

const ne::Vector2i& TiledParser::getSize() const
{
    return size_;
}

const ne::Vector2i& TiledParser::getTileSize() const
{
    return tile_size_;
}

const int TiledParser::getSpacing() const
{
    return spacing_;
}

int TiledParser::getNumberOfLayers() const
{
    return number_of_layers_;
}

int** TiledParser::getTiles(int layer)
{
    return tiles_[layer];
}

}
