#include "Neru/EntityData.h"

namespace ne
{

EntityData::EntityData()
{
    last_created_animation_ = -1;
    useInterpolation(true);
}

EntityData::EntityData(int spritesheet_frame_width, int spritesheet_frame_height, int spritesheet_frame_spacing) : ne::SpriteSheet(spritesheet_frame_width,spritesheet_frame_height,spritesheet_frame_spacing)
{
    last_created_animation_ = -1;
    useInterpolation(true);
}

void EntityData::newAnimation(const std::string &animation_name, int start_frame, int end_frame, int timing)
{
    // Increment animation counter
    last_created_animation_++;

    // Add Name to Map
    name_map_.insert(std::pair<std::string,int>(animation_name,last_created_animation_));

    // Setup New Animation
    AnimationData new_animation;

    new_animation.range = ne::Vector2i(start_frame,end_frame);
    new_animation.active_range = ne::Vector2i(0,0);

    const int size = end_frame - start_frame + 1;
    new_animation.timing.resize(size);
    new_animation.frame_displacement.resize(size);
    new_animation.active_region.resize(size);

    // Add default data
    for (int i = 0; i < size; i++)
    {
        new_animation.timing[i] = timing;
        new_animation.frame_displacement[i] = ne::Vector2i(0,0);
        new_animation.active_region[i] = ne::Rectf(0,0,0,0);
    }

    // Animations are looped by default
    new_animation.loop = true;

    animation_.push_back(new_animation);
}

int EntityData::convertToRow(int frame) const
{
    return static_cast<int>((static_cast<float>(frame - 1)/getSpritesPerRow()) + 1);
}

int EntityData::convertToColumn(int frame) const
{
    return frame - ((convertToRow(frame) - 1)*getSpritesPerRow());
}

int EntityData::convertToFrame(const std::string &animation_name, int normalized_frame) const
{
    int index;

    if (!animationIsValid(animation_name,&index))
        return 1;

    return normalized_frame + animation_[index].range.x - 1;
}

bool EntityData::animationIsValid(const std::string &animation_name, int *index) const
{
    const int check = name_map_.at(animation_name);

    if (check < 0 || check > last_created_animation_)
    {
        std::cout << "Error: Invalid index from map when trying to receive \"" << animation_name << "\".\n";
        return false;
    }

    *index = check;
    return true;
}

bool EntityData::frameIsValid(int frame, int animation_index) const
{
    if (frame < animation_[animation_index].range.x || frame > animation_[animation_index].range.y)
    {
        std::cout << "Error: frame is out of range on given index \"" << animation_index << "\".\n";
        return false;
    }

    return true;
}

bool EntityData::nframeIsValid(int nframe, int animation_index) const
{
    if (nframe < 1 || nframe > (animation_[animation_index].range.y - animation_[animation_index].range.x + 1))
    {
        std::cout << "Error: normalized frame is out of range on given index \"" << animation_index << "\".\n";
        return false;
    }

    return true;
}

// Setters

void EntityData::setBaseDisplacement(int x, int y, int hitbox_width_of_character)
{
    base_displacement_ = ne::Vector3i(x, y, hitbox_width_of_character - x - getSpriteBoxWidth() - 1);
}

void EntityData::displaceAnimation(int x, int y)
{
    for (unsigned int i = 0; i < animation_[last_created_animation_].frame_displacement.size(); i++)
    {
        animation_[last_created_animation_].frame_displacement[i] = ne::Vector2i(x,y);
    }
}

void EntityData::displaceFrame(int frame, int x, int y)
{
    if (!frameIsValid(frame,last_created_animation_))
        return;

    // Normalize frame
    frame -= animation_[last_created_animation_].range.x;

    animation_[last_created_animation_].frame_displacement[frame] = ne::Vector2i(x,y);
}

void EntityData::setAnimationTiming(int timing)
{
    for(unsigned int i = 0; i < animation_[last_created_animation_].timing.size(); i++)
    {
        animation_[last_created_animation_].timing[i] = timing;
    }
}

void EntityData::setFrameTiming(int frame, int timing)
{
    if (!frameIsValid(frame,last_created_animation_))
        return;

    // Normalize frame
    frame -= animation_[last_created_animation_].range.x;

    animation_[last_created_animation_].timing[frame] = timing;
}

void EntityData::setLoop(bool b)
{
    animation_[last_created_animation_].loop = b; 
}

void EntityData::setActiveFrames(int frame_start, int frame_end)
{
    animation_[last_created_animation_].active_range = ne::Vector2i(frame_start,frame_end);
}

void EntityData::setActiveRegion(int frame, const ne::Rectf &region)
{
    if (!frameIsValid(frame,last_created_animation_))
        return;

    // Normalize frame
    frame -= animation_[last_created_animation_].range.x;

    animation_[last_created_animation_].active_region[frame] = region;
}

// Getters

bool EntityData::animationExists(const std::string &animation_name) const
{
    return name_map_.count(animation_name) > 0;
}

int EntityData::getAnimationIndex(const std::string &animation_name) const
{
    int index;

    if (!animationIsValid(animation_name,&index))
        return 0;

    return index;
}

const ne::Vector3i& EntityData::getBaseDisplacement() const
{
    return base_displacement_;
}

ne::Vector2i EntityData::getFrameDisplacement(const std::string &animation_name, int nframe) const
{
    int index;

    if (!animationIsValid(animation_name,&index))
        return ne::Vector2i(0,0);

    if (!nframeIsValid(nframe,index))
        return ne::Vector2i(0,0);

    return animation_[index].frame_displacement[nframe-1];
}

int EntityData::getFrameTiming(const std::string &animation_name, int nframe) const
{
    int index;

    if (!animationIsValid(animation_name,&index))
        return 0;

    if (!nframeIsValid(nframe,index))
        return 0;

    return animation_[index].timing[nframe-1];
}

bool EntityData::getLoop(const std::string &animation_name) const
{
    int index;

    if (!animationIsValid(animation_name,&index))
        return false;

    return animation_[index].loop;
}

unsigned int EntityData::getNumberOfFrames(const std::string &animation_name) const
{
    int index;

    if (!animationIsValid(animation_name,&index))
        return 0;

    return animation_[index].timing.size();
}

ne::Vector2i EntityData::getFrameRange(const std::string &animation_name) const
{
    int index;

    if (!animationIsValid(animation_name,&index))
        return ne::Vector2i(0,0);

    return animation_[index].range;
}

bool EntityData::isActive(const std::string &animation_name, int nframe) const
{
    int index;

    if (!animationIsValid(animation_name,&index))
        return false;

    if (!nframeIsValid(nframe,index))
        return false;

    // Normalize range down to 1...x for comparison
    const int start = animation_[index].active_range.x - animation_[index].range.x + 1;
    const int end = animation_[index].active_range.y - animation_[index].range.x + 1;

    return nframe >= start && nframe <= end;
}

ne::Vector2i EntityData::getActiveRange(const std::string &animation_name) const
{
    int index;

    if (!animationIsValid(animation_name,&index))
        return ne::Vector2i(0,0);

    return animation_[index].active_range;
}

ne::Rectf EntityData::getActiveRegion(const std::string &animation_name, int nframe) const
{
    int index;

    if (!animationIsValid(animation_name,&index))
        return ne::Rectf(0,0,0,0);

    if (!nframeIsValid(nframe,index))
        return ne::Rectf(0,0,0,0);

    return animation_[index].active_region[nframe-1];
}

}
