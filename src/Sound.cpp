#include "Neru/Sound.h"

namespace ne
{

Sound::Sound()
{
    chunk_ = NULL;
    zip_rw_ = NULL;
    zip_used_ = false;
    channel_ = -1;
    volume_ = MIX_MAX_VOLUME;
}

Sound::~Sound()
{
    deconstruct();
}

void Sound::deconstruct()
{
    stop();
    Mix_FreeChunk(chunk_);
    if(zip_used_)
    {
        free(zip_sound_buffer_);
        SDL_RWclose(zip_rw_);
        zip_used_ = false;
    }
}

void Sound::loadZipWAV(const std::string &file_path)
{
    deconstruct();

    ne::Zip zip(file_path);
    zip_sound_buffer_ = zip.getBuffer();

    zip_rw_ = SDL_RWFromMem(zip.getBuffer(),zip.getSize());
    chunk_ = Mix_LoadWAV_RW(zip_rw_,0);
    
    if (chunk_ == NULL)
        std::cout << "Error loading \"" << file_path << "\" from ZIP\n";

    file_name_ = file_path.substr(file_path.find_last_of("/") + 1);
    zip_used_ = true;
}

void Sound::loadFileWAV(const std::string &file_path)
{
    deconstruct();
    
    chunk_ = Mix_LoadWAV(file_path.c_str());
    
    if (chunk_ == NULL)
        std::cout << "Error loading \"" << file_path << "\" from file\n";
    
    file_name_ = file_path.substr(file_path.find_last_of("/") + 1);
    zip_used_ = false;
}

void Sound::play()
{
    channel_ = Mix_PlayChannel(-1, chunk_, 0);
    
    if (channel_ == -1)
        std::cout << Mix_GetError() << "\n";
    else
        Mix_Volume(channel_,volume_);
}

void Sound::stop()
{
    if (channel_ != -1)
        Mix_HaltChannel(channel_);
}

// Setters
    
void Sound::setVolume(int volume)
{
    if (volume > MIX_MAX_VOLUME) volume = MIX_MAX_VOLUME;
    else if (volume < 0) volume = 0;
    
    volume_ = volume;
}

// Getters

int Sound::getVolume() const
{
    return volume_;
}

const std::string& Sound::getFileName() const
{
    return file_name_;
}

}
