#include "Neru/ImageAtlas.h"

namespace ne
{

ImageAtlas::ImageAtlas()
{

}

void ImageAtlas::add(const ne::Recti &rect)
{
    data_.push_back(rect);
}

void ImageAtlas::setTextureRect(unsigned int atlas_id)
{
    if (!data_.empty() && atlas_id < data_.size())
        ne::Image::setTextureRect(data_[atlas_id]);
}

void ImageAtlas::clear()
{
    data_.clear();
}

// Getters

const ne::Recti& ImageAtlas::getData(unsigned int atlas_id) const
{
	return data_[atlas_id];
}

unsigned int ImageAtlas::getSize() const
{
    return data_.size();
}

}
