#include "Neru/Image.h"

namespace ne
{

SDL_Renderer * Image::internal_renderer_ = NULL;

Image::Image()
{
    setPosition(0,0);
    original_width_ = 0;
    original_height_ = 0;
    angle_ = 0;
    scale_.x = 1.0f;
    scale_.y = 1.0f;
    loaded_texture_ = false;
    use_interpolation_ = false;
    flip_ = false;
    target_rect_ = ne::toSDLRect(0,0,0,0);
    texture_rect_ = ne::toSDLRect(0,0,0,0);
}

Image::Image(const ne::Vector2f &position)
{
    setPosition(position);
    original_width_ = 0;
    original_height_ = 0;
    angle_ = 0;
    scale_.x = 1.0f;
    scale_.y = 1.0f;
    loaded_texture_ = false;
    use_interpolation_ = false;
    flip_ = false;
    target_rect_ = ne::toSDLRect(0,0,0,0);
    texture_rect_ = ne::toSDLRect(0,0,0,0);
}

Image::Image(float x, float y)
{
    setPosition(x,y);
    original_width_ = 0;
    original_height_ = 0;
    angle_ = 0;
    scale_.x = 1.0f;
    scale_.y = 1.0f;
    loaded_texture_ = false;
    use_interpolation_ = false;
    flip_ = false;
    target_rect_ = ne::toSDLRect(0,0,0,0);
    texture_rect_ = ne::toSDLRect(0,0,0,0);
}

Image::~Image()
{
    deconstruct();
}

void Image::deconstruct()
{
    if (loaded_texture_)
    {
        SDL_DestroyTexture(texture_);
        loaded_texture_ = false;
    }
}

void Image::loadZipImage(const std::string &file_path)
{
    deconstruct();
    
    ne::Zip zip(file_path);
    
    SDL_RWops *rw = SDL_RWFromMem(zip.getBuffer(),zip.getSize());
    SDL_Surface *surface = IMG_Load_RW(rw,0);
    
    if (surface != NULL)
    {
        file_name_ = file_path.substr(file_path.find_last_of("/") + 1);
        finishLoading(surface);
        SDL_FreeSurface(surface);
    }
    else std::cout << "Error Loading \"" << file_path << "\" from ZIP\n";
    
    free(zip.getBuffer());
    SDL_RWclose(rw);
}

void Image::loadFileImage(const std::string &file_path)
{
    deconstruct();
    
    SDL_Surface *surface = IMG_Load(file_path.c_str());
    
    if (surface != NULL)
    {
        file_name_ = file_path.substr(file_path.find_last_of("/") + 1);
        finishLoading(surface);
        SDL_FreeSurface(surface);
    }
    else std::cout << "Error loading \"" << file_path << "\" from file\n";
}

void Image::finishLoading(SDL_Surface *surface)
{
    texture_ = SDL_CreateTextureFromSurface(internal_renderer_,surface);
    SDL_SetTextureBlendMode(texture_, SDL_BLENDMODE_BLEND);
    target_rect_.w = surface->w;
    target_rect_.h = surface->h;
    texture_rect_.x = 0;
    texture_rect_.y = 0;
    texture_rect_.w = surface->w;
    texture_rect_.h = surface->h;
    original_width_ = surface->w;
    original_height_ = surface->h;
    loaded_texture_ = true;
}

void Image::updateLast()
{
    last_position_ = getPosition();
}

void Image::updateLast(float x, float y)
{
    last_position_ = ne::Vector2f(x,y);
}

void Image::updateLast(const ne::Vector2f &last_position)
{
    last_position_ = last_position;
}

void Image::setTextureRect(const ne::Recti &rect)
{
    texture_rect_ = ne::toSDLRect(rect);
    target_rect_.w = texture_rect_.w;
    target_rect_.h = texture_rect_.h;
}

void Image::updateTarget()
{
    target_rect_.x = ne::round(position_.x);
    target_rect_.y = ne::round(position_.y);
}

// Setters

void Image::useInterpolation(bool use)
{
    use_interpolation_ = use;
}

void Image::setTexture(ne::Image *texture)
{
    texture_ = texture->getTexture();
    target_rect_.w = texture->getW();
    target_rect_.h = texture->getH();
    texture_rect_.x = 0;
    texture_rect_.y = 0;
    texture_rect_.w = texture->getW();
    texture_rect_.h = texture->getH();
    original_width_ = texture->getW();
    original_height_ = texture->getH();
}

void Image::setTexture(SDL_Texture *texture, int width, int height)
{
    texture_ = texture;
    target_rect_.w = width;
    target_rect_.h = height;
    texture_rect_.x = 0;
    texture_rect_.y = 0;
    texture_rect_.w = width;
    texture_rect_.h = height;
    original_width_ = width;
    original_height_ = height;
}

void Image::setPosition(const ne::Vector2f &position, bool centre)
{
    if (centre)
        position_ = position - ne::Vector2f(getW() / 2.0f, getH() / 2.0f);
    else
        position_ = position;

    updateTarget();
}
    
void Image::setPosition(float x, float y, bool centre)
{
    if (centre)
    {
        position_.x = x - (getW() / 2.0f);
        position_.y = y - (getH() / 2.0f);
    }
    else
    {
        position_.x = x;
        position_.y = y;
    }

    updateTarget();
}

void Image::setX(float x)
{
    position_.x = x;
    updateTarget();
}

void Image::setY(float y)
{
    position_.y = y;
    updateTarget();
}

void Image::setColourMod(const SDL_Color &colour)
{
    colour_ = colour;
    SDL_SetTextureColorMod(texture_,colour.r,colour.g,colour.b);
    SDL_SetTextureAlphaMod(texture_,colour.a);
}

void Image::setAlphaMod(int a)
{
    if (a > 255)    a = 255;
    else if (a < 0) a = 0;
    
    colour_.a = a;
    SDL_SetTextureAlphaMod(texture_, a);
}

void Image::setRotation(float angle)
{
    angle_ = angle;
}

void Image::setScaling(const ne::Vector2f &scale)
{
    scale_ = scale;
}

void Image::setFlip(bool flip)
{
    flip_ = flip;
}

void Image::setInternalRenderer(SDL_Renderer *renderer)
{
    internal_renderer_ = renderer;
}

// Getters

bool Image::isInterpolated() const
{
    return use_interpolation_;
}

SDL_Texture* Image::getTexture()
{
    return texture_;
}

const ne::Vector2f& Image::getPosition() const
{
    return position_;
}

SDL_Rect* Image::getTargetRect()
{
    return &target_rect_;
}

SDL_Rect* Image::getTextureRect()
{
    return &texture_rect_;
}

float Image::getX() const
{
    return position_.x;
}

float Image::getY() const
{
    return position_.y;
}

int Image::getW() const
{
    return texture_rect_.w;
}

int Image::getH() const
{
    return texture_rect_.h;
}

int Image::getImageW() const
{
    return original_width_;
}

int Image::getImageH() const
{
    return original_height_;
}

const SDL_Color& Image::getColourMod() const
{
    return colour_;
}

int Image::getAlphaMod() const
{
    return colour_.a;
}

float Image::getRotation() const
{
    return angle_;
}

const ne::Vector2f& Image::getScaling() const
{
    return scale_;
}

bool Image::getFlip() const
{
    return flip_;
}

const ne::Vector2f& Image::getLastPosition() const
{
    return last_position_;
}

ne::Vector2f Image::getInterpolatedPosition(float interpolation) const
{
    return ne::Vector2f(getPosition().x*interpolation + last_position_.x*(1.0f - interpolation),
                        getPosition().y*interpolation + last_position_.y*(1.0f - interpolation));
}

const std::string& Image::getFileName() const
{
    return file_name_;
}

}
