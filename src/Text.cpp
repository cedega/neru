#include "Neru/Text.h"

namespace ne
{

SDL_Renderer * Text::internal_renderer_ = NULL;

Text::Text()
{
    font_ = NULL;
    texture_ = NULL;
    font_loaded_ = false;
    zip_rw_ = NULL;
    zip_font_loaded_ = false;
    use_interpolation_ = false;
    size_ = 16;
    colour_ = ne::toSDLColor(255,255,255);
    target_rect_ = ne::toSDLRect(0,0,0,0);
    texture_rect_ = ne::toSDLRect(0,0,0,0);
    scale_ = ne::Vector2f(1.0f, 1.0f);
    wrap_ = 0;
}

Text::~Text()
{
    deconstruct();
}

void Text::deconstruct()
{
    if (font_loaded_)
        TTF_CloseFont(font_);
    
    if (texture_ != NULL)
        SDL_DestroyTexture(texture_);
        
    if (zip_font_loaded_)
    {
        free(zip_font_buffer_);
        SDL_RWclose(zip_rw_);
        zip_font_loaded_ = false;
    }
}

void Text::loadZipFont(const std::string &file_path, int character_size)
{
    deconstruct();
    
    ne::Zip zip(file_path);
    zip_font_buffer_ = zip.getBuffer();

    zip_rw_ = SDL_RWFromMem(zip.getBuffer(),zip.getSize());
    font_ = TTF_OpenFontRW(zip_rw_,0,character_size);
    size_ = character_size;

    if (font_ == NULL)
        std::cout << "Error loading \"" << file_path << "\" from ZIP file\n";
        
    setString(string_);

    font_loaded_ = true;
    zip_font_loaded_ = true;
}

void Text::loadFileFont(const std::string &file_path, int character_size)
{
    deconstruct();
    
    font_ = TTF_OpenFont(file_path.c_str(),character_size);
    size_ = character_size;
    
    if (font_ == NULL)
        std::cout << "Error loading \"" << file_path << "\" from file\n";
    
    setString(string_);
    
    font_loaded_ = true;
    zip_font_loaded_ = false;
}

void Text::updateLast()
{
    last_position_ = position_;
}

void Text::updateLast(float x, float y)
{
    last_position_ = ne::Vector2f(x,y);
}

void Text::updateLast(const ne::Vector2f &last_position)
{
    last_position_ = last_position;
}

void Text::updateTarget()
{
    // Implicit integer casting
    target_rect_.x = position_.x;
    target_rect_.y = position_.y;
}

// Setters

void Text::useInterpolation(bool use)
{
    use_interpolation_ = use;
}

void Text::setFont(TTF_Font *font)
{
    font_ = font;
    setString(string_);
    size_ = 0;
}

void Text::setPosition(const ne::Vector2f &position)
{
    position_ = position;
    updateTarget();
}

void Text::setScaling(const ne::Vector2f &scale)
{
    scale_ = scale;
}

void Text::setX(float x)
{
    position_.x = x;
    updateTarget();
}

void Text::setY(float y)
{
    position_.y = y;
    updateTarget();
}

void Text::setString(const std::string &string)
{
    if (font_ == NULL)
        return;
        
    if (texture_ != NULL)
        SDL_DestroyTexture(texture_);

    string_ = string;
    SDL_Surface *surface = NULL;

    if (wrap_ > 0)
        surface = TTF_RenderText_Blended_Wrapped(font_, string_.c_str(), colour_, wrap_);
    else
        surface = TTF_RenderText_Blended(font_, string_.c_str(), colour_);
    
    if (surface != NULL)
    {
        target_rect_.w = surface->w;
        target_rect_.h = surface->h;
        texture_rect_.x = 0;
        texture_rect_.y = 0;
        texture_rect_.w = surface->w;
        texture_rect_.h = surface->h;
        texture_ = SDL_CreateTextureFromSurface(internal_renderer_, surface);
        SDL_FreeSurface(surface);
    }
    else 
    {
        // Failed to create surface -- most likely a blank string
        target_rect_.w = 0;
        target_rect_.h = 0;
        texture_rect_.x = 0;
        texture_rect_.y = 0;
        texture_rect_.w = 0;
        texture_rect_.h = 0;
        texture_ = NULL;
    }
}

void Text::setColour(const SDL_Color &colour)
{
    colour_ = colour;
    setString(string_);
}

void Text::setInternalRenderer(SDL_Renderer *renderer)
{
    internal_renderer_ = renderer;
}

void Text::setWrap(unsigned int wrap)
{
    wrap_ = wrap;
}

// Getters

bool Text::isInterpolated() const
{
    return use_interpolation_;
}

TTF_Font* Text::getFont()
{
    return font_;
}

SDL_Texture* Text::getTexture()
{
    return texture_;
}

const ne::Vector2f& Text::getPosition() const
{
    return position_;
}

const ne::Vector2f& Text::getScaling() const
{
    return scale_;
}

SDL_Rect* Text::getTargetRect()
{
    return &target_rect_;
}

int Text::getCharacterSize() const
{
    return size_;
}

SDL_Rect* Text::getTextureRect()
{
    return &texture_rect_;
}

float Text::getX() const
{
    return position_.x;
}

float Text::getY() const
{
    return position_.y;
}

int Text::getW() const
{
    return texture_rect_.w;
}

int Text::getH() const
{
    return texture_rect_.h;
}

const ne::Vector2f& Text::getLastPosition() const
{
    return last_position_;
}

ne::Vector2f Text::getInterpolatedPosition(float interpolation) const
{
    return ne::Vector2f(position_.x*interpolation + last_position_.x*(1.0f - interpolation),
                        position_.y*interpolation + last_position_.y*(1.0f - interpolation));
}

const std::string& Text::getString() const
{
    return string_;
}

const SDL_Color& Text::getColour() const
{
    return colour_;
}

unsigned int Text::getWrap() const
{
    return wrap_;
}

}
