#include "Neru/SpriteSheet.h"

namespace ne
{

SpriteSheet::SpriteSheet()
{
    sprite_box_width_ = 0;
    sprite_box_height_ = 0;
    sprite_spacing_ = 0;
    row_ = 1;
    column_ = 1;
}

SpriteSheet::SpriteSheet(int sprite_box_width, int sprite_box_height, int sprite_spacing)
{
    sprite_box_width_ = sprite_box_width;
    sprite_box_height_ = sprite_box_height;
    sprite_spacing_ = sprite_spacing;
    row_ = 1;
    column_ = 1;
}

int SpriteSheet::addColumnSpacing(int c) const
{
    return c*(sprite_box_width_+sprite_spacing_) + sprite_spacing_;
}

int SpriteSheet::addRowSpacing(int r) const
{
    return r*(sprite_box_height_+sprite_spacing_) + sprite_spacing_;
}

void SpriteSheet::setSprite(int row, int column)
{
    row_ = row;
    column_ = column;

    setTextureRect(ne::Recti(addColumnSpacing(column-1),addRowSpacing(row-1),sprite_box_width_,sprite_box_height_));
}

//Setters

void SpriteSheet::setSpriteSheet(int sprite_box_width, int sprite_box_height, int sprite_spacing)
{
    sprite_box_width_ = sprite_box_width;
    sprite_box_height_ = sprite_box_height;
    sprite_spacing_ = sprite_spacing;
}

//Getters

int SpriteSheet::getRow() const
{
    return row_;
}

int SpriteSheet::getColumn() const
{
    return column_;
}

int SpriteSheet::getSpriteBoxWidth() const
{
    return sprite_box_width_;
}

int SpriteSheet::getSpriteBoxHeight() const
{
    return sprite_box_height_;
}

int SpriteSheet::getSpriteSpacing() const
{
    return sprite_spacing_;
}

int SpriteSheet::getSpritesPerRow() const
{
    return getImageW()/static_cast<float>(sprite_box_width_ + sprite_spacing_);
}

ne::Recti SpriteSheet::getBounds(int row, int column) const
{
    ne::Recti temp;
    temp.x = addColumnSpacing(column-1);
    temp.y = addRowSpacing(row-1);
    temp.w = sprite_box_width_;
    temp.h = sprite_box_height_;

    return temp;
}

}
