#include "Neru/Zip.h"

namespace ne
{

std::string Zip::zip_path_;
std::string Zip::zip_password_;

Zip::Zip()
{
    zip_ = NULL;
}

Zip::Zip(const std::string &filename)
{
    zip_ = NULL;
    loadFile(filename);
}

void Zip::loadFile(const std::string &filename)
{
    zip_ = unzOpen(zip_path_.c_str());
    
    if (zip_ == NULL)
    { 
        std::cout << "Error opening Zip\n";
    }
    unzLocateFile(zip_,filename.c_str(),0);
    
    unzGetCurrentFileInfo(zip_,&info_,NULL,0,NULL,0,NULL,0);
    
    //unzOpenCurrentFile(zip_); unencrypted file load
    if (unzOpenCurrentFilePassword(zip_,zip_password_.c_str()) != UNZ_OK)
    {
        std::cout << "ne::Zip Decrypt Failure.\n";
    }

    buffer_ = (char*)malloc(info_.uncompressed_size);

    if ((unsigned int)unzReadCurrentFile(zip_, buffer_, info_.uncompressed_size) != info_.uncompressed_size)
    {
        std::cout << "ne::Zip Compression Error\n";
    }

    size_ = info_.uncompressed_size;
    unzClose(zip_);
}

// Static

void Zip::setZipPath(const std::string &path)
{
    zip_path_ = path;
}

void Zip::setZipPassword(const std::string &password)
{
    zip_password_ = password;
}

// Getters

char* Zip::getBuffer()
{
    return buffer_;
}

uLong Zip::getSize()
{
    return size_;
}

}
