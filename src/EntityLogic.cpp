#include "Neru/EntityLogic.h"

namespace ne
{

float EntityLogic::default_gravity_ = 2500.0f;
ne::Vector2f EntityLogic::default_terminal_velocity_ = ne::Vector2f(3000, 800);
float EntityLogic::physics_dt_ = 1.0f / 60.0f;

EntityLogic::EntityLogic()
{
    num_forces_x_ = 0;
    num_forces_y_ = 0;
	ignore_gravity_ = false;

    gravity_ = getDefaultGravity();
    terminal_velocity_ = getDefaultTerminalVelocity();
}

// Velocity is seperated into 2 variables: force and non-forced
void EntityLogic::move(const float dt, bool rerun)
{
    // Compute force velocities
    applyForces(dt,rerun);

	// Compute Gravity
	if (!ignore_gravity_ && !forceHasDisabledGravity())
		acceleration_.y = gravity_;
	else
		acceleration_.y = 0;

    // Apply acceleration before position value calculation
    velocity_ += acceleration_*dt; 

    // Move
    step_ = velocity_*dt + force_velocity_*dt;

    hitbox_.x = hitbox_.x + step_.x;
    hitbox_.y = hitbox_.y + step_.y;
    
	// Set terminal velocities
    if (ne::absval(velocity_.y) > terminal_velocity_.y)
    {
        if (velocity_.y < 0) velocity_.y = -terminal_velocity_.y;
        else                 velocity_.y = terminal_velocity_.y;
    }

    if (ne::absval(velocity_.x) > terminal_velocity_.x)
    {
        if (velocity_.y < 0) velocity_.x = -terminal_velocity_.x;
        else                 velocity_.x = terminal_velocity_.x;
    }
}

// Force acceleration is continually added to the internal force velocity
// therefore, setting force variables to 0 every frame does not break acceleration build up
void EntityLogic::applyForces(float dt, bool rerun)
{
    // Set the force velocities to 0 -- prepare for adding
    force_velocity_ = ne::Vector2f(0,0);

    // X-Forces
    for (int i = 0; i < ne::MAX_FORCES; i++)
    {
        // If Force is active
        if (forces_x_[i].active && (!rerun || forces_x_[i].rerun))
        {
            forces_x_[i].time -= static_cast<int>(dt*1000);

            // As long as the force is still active, then move
            if (forces_x_[i].time > 0)
            {
                force_velocity_.x += forces_x_[i].velocity;
                forces_x_[i].velocity += forces_x_[i].acceleration*dt;
            }
            else
            {
                // remove force
                onForceXEnd(i);
                forces_x_[i].active = false;
                num_forces_x_--;
            }
        }
    }

    // Y-Forces
    for (int i = 0; i < ne::MAX_FORCES; i++)
    {
        // If Force is active
        if (forces_y_[i].active && (!rerun || forces_y_[i].rerun))
        {
            forces_y_[i].time -= static_cast<int>(dt*1000);

            // As long as the force is still active, then move
            if (forces_y_[i].time > 0)
            {
                force_velocity_.y += forces_y_[i].velocity;
                forces_y_[i].velocity += forces_y_[i].acceleration*dt; 
            }
            else
            {
                // remove force
                onForceYEnd(i);
                forces_y_[i].active = false;
                num_forces_y_--;
            }
        }
    }
}

int EntityLogic::collide(ne::Rectf *A, const ne::Rectf *B)
{
    const float bump = 0.0001f;

	if (ne::intersects(*A,*B))
	{ 
        ne::Vector2f A_centre(A->x + A->w/2.0f, A->y + A->h/2.0f);
        ne::Vector2f B_centre(B->x + B->w/2.0f, B->y + B->h/2.0f);

		float difference_x = A_centre.x > B_centre.x ? (B->x + B->w) - A->x : (A->x + A->w) - B->x;
		float difference_y = A_centre.y < B_centre.y ? (A->y + A->h) - B->y : (B->y + B->h) - A->y;
        
        if (difference_x < 0)
            difference_x = FLT_MAX;
            
        if (difference_y < 0)
            difference_y = FLT_MAX;

        // If colliding from right moving left 
		if (A_centre.x > B_centre.x)
		{
            // If it takes less effort to fix on the x-axis 
			if (difference_x <= difference_y && difference_y > gravity_*physics_dt_*physics_dt_)
            {                                    // This is so slow-moving entities don't get stuck on the map
				A->x = A->x + difference_x + bump;
                return 1;
			}
            // If it takes less effort to fix on the y-axis
			else
			{
                // If colliding from the top moving down 
				if (A_centre.y < B_centre.y)
				{
                    // only apply upwards collision if the player is "falling"
                    if (step_.y >= 0)
                    {
                        A->y = A->y - difference_y - bump;
                        
                        setVelocity(ne::Vector2f(getVelocity().x,0));
                        onLand();
                        return 2;
                    }
                    // Must displace else the collision isn't fixed
                    else
                    {
                        A->x = A->x + difference_x + bump;
                        return 1;
                    }
				}
                // If colliding from the bottom moving up
				else
				{
                    A->y = A->y + difference_y + bump;
                    
                    // Make velocity only get reset when you hit your head from below (aka, in a jump)
                    if (step_.y < 0)
                        setVelocity(ne::Vector2f(getVelocity().x,0));
                    return 3;
				}
			}
		}
        // If colliding from the left moving right
		else
		{
            // If it takes less effort to fix on the x-axis
			if (difference_x <= difference_y && difference_y > gravity_*physics_dt_*physics_dt_)
			{                                    // This is so slow-moving entities don't get stuck on the map
                A->x = A->x - difference_x - bump;
                return 4;
			}
            // If it takes less effort to fix on the y-axis
			else
			{
                // If colliding from the top moving down
				if (A_centre.y < B_centre.y)
				{
                    // only apply upwards collision if the player is "falling"
                    if (step_.y >= 0) 
                    {
                        A->y = A->y - difference_y - bump;
                        
                        setVelocity(ne::Vector2f(getVelocity().x,0));
                        onLand();
                        return 5;
                    }
                    // Must displace else the collision isn't fixed
                    else
                    {
                        A->x = A->x - difference_x - bump;
                        return 4;
                    }
				}
                // If colliding from the bottom moving up
				else
				{
                    A->y = A->y + difference_y + bump;

                    // Make velocity only get reset when you hit your head from below (aka, in a jump)
                    if (step_.y < 0)
                        setVelocity(ne::Vector2f(getVelocity().x,0));
                    return 6;
				}
			}
		}
	}

    return 0;
}

// Forces

int EntityLogic::addForceX(float velocity, float acceleration, int time)
{
    int force_index = -1;

    // Find unoccupied force
    for (int i = 0; i < ne::MAX_FORCES; i++)
    {
        // If not active)
        if (!forces_x_[i].active)
        {
            force_index = i;
            break;
        }
    }

    if (force_index != -1)
    {
        forces_x_[force_index].active = true;
        forces_x_[force_index].rerun = true;
        forces_x_[force_index].disable_gravity = false;
        forces_x_[force_index].velocity = velocity;
        forces_x_[force_index].acceleration = acceleration;
        forces_x_[force_index].time = time;

        num_forces_x_++;
    }
    else
    {
        //std::cout << "Report: Force List is Full\n";
    }
	
	return force_index;
}

int EntityLogic::addForceY(float velocity, float acceleration, int time)
{
    int force_index = -1;

    // Find unoccupied force
    for (int i = 0; i < ne::MAX_FORCES; i++)
    {
        // If not active
        if (!forces_y_[i].active)
        {
            force_index = i;
            break;
        }
    }

    if (force_index != -1)
    {
        forces_y_[force_index].active = true;
        forces_y_[force_index].rerun = true;
        forces_y_[force_index].disable_gravity = false;
        forces_y_[force_index].velocity = velocity;
        forces_y_[force_index].acceleration = acceleration;
        forces_y_[force_index].time = time;

        num_forces_y_++;
    }
    else
    {
        //std::cout << "Report: Force List is Full\n";
    }
	
	return force_index;
}

int EntityLogic::addForceX(const ne::Force &force)
{
    return addForceX(force.velocity,force.acceleration,force.time);
}

int EntityLogic::addForceY(const ne::Force &force)
{
    return addForceY(force.velocity,force.acceleration,force.time);
}

void EntityLogic::removeForceX(int force_id)
{
    if (force_id >= 0)
    {
        if (forces_x_[force_id].active)
        {
            onForceXEnd(force_id);
            forces_x_[force_id].active = false;
            num_forces_x_--;
        }
    }
}

void EntityLogic::removeForceY(int force_id)
{
    if (force_id >= 0)
    {
        if (forces_y_[force_id].active)
        {
            onForceYEnd(force_id);
            forces_y_[force_id].active = false;
            num_forces_y_--;
        }
    }
}

void EntityLogic::removeAllForces()
{
    for (int i = 0; i < ne::MAX_FORCES; i++)
    {
        if (forces_y_[i].active)
            onForceYEnd(i);
        if (forces_x_[i].active)
            onForceXEnd(i);

        forces_y_[i].active = false;
        forces_x_[i].active = false;
    }
    num_forces_y_ = 0;
    num_forces_x_ = 0;
}

bool EntityLogic::forceExistsX(int force_id) const
{
	if (force_id >= 0)
		return forces_x_[force_id].active;
	else
		return false;
}

bool EntityLogic::forceExistsY(int force_id) const
{
	if (force_id >= 0)
		return forces_y_[force_id].active;
	else
		return false;
}

bool EntityLogic::anyForceExistsX() const
{
    for (int i = 0; i < ne::MAX_FORCES; i++)
        if (forces_x_[i].active)
            return true;

    return false;
}

bool EntityLogic::anyForceExistsY() const
{
    for (int i = 0; i < ne::MAX_FORCES; i++)
        if (forces_y_[i].active)
            return true;

    return false;
}

bool EntityLogic::forceHasDisabledGravity() const
{
    for (int i = 0; i < ne::MAX_FORCES; i++)
    {
        if (forces_x_[i].active && forces_x_[i].disable_gravity)
            return true;
        if (forces_y_[i].active && forces_y_[i].disable_gravity)
            return true;
    }

    return false;
}

int EntityLogic::applyNoRerunX(int force_id)
{
    if (force_id >= 0)
        forces_x_[force_id].rerun = false;

    return force_id;
}

int EntityLogic::applyNoRerunY(int force_id)
{
    if (force_id >= 0)
        forces_y_[force_id].rerun = false;

    return force_id;
}

int EntityLogic::applyNoGravityX(int force_id)
{
    if (force_id >= 0)
        forces_x_[force_id].disable_gravity = true;

    return force_id;
}

int EntityLogic::applyNoGravityY(int force_id)
{
    if (force_id >= 0)
        forces_y_[force_id].disable_gravity = true;

    return force_id;
}

// Triggers

void EntityLogic::onForceXEnd(int force_id)
{

}

void EntityLogic::onForceYEnd(int force_id)
{

}

void EntityLogic::onLand()
{

}

// Static

void EntityLogic::setDefaultGravity(float gravity)
{
    default_gravity_ = gravity;
}

void EntityLogic::setDefaultTerminalVelocity(const ne::Vector2f &terminal_velocity)
{
    default_terminal_velocity_ = terminal_velocity;
}

float EntityLogic::getDefaultGravity()
{
    return default_gravity_;
}

ne::Vector2f EntityLogic::getDefaultTerminalVelocity()
{
    return default_terminal_velocity_;
}

void EntityLogic::setPhysicsDt(float dt)
{
    physics_dt_ = dt;
}

// Setters

void EntityLogic::setHitbox(const ne::Vector2f &h)
{
    hitbox_.w = h.x;
    hitbox_.h = h.y;
}

void EntityLogic::setX(float x)
{
    hitbox_.x = x;
}

void EntityLogic::setY(float y)
{
    hitbox_.y = y;
}

void EntityLogic::setPosition(const ne::Vector2f &position)
{
    hitbox_.x = position.x;
    hitbox_.y = position.y;
}

void EntityLogic::setVelocity(const ne::Vector2f &velocity)
{
    velocity_ = velocity;
}

void EntityLogic::setAcceleration(const ne::Vector2f &acceleration)
{
    acceleration_ = acceleration;
}

void EntityLogic::setIgnoreGravity(bool ignore)
{
	ignore_gravity_ = ignore;
}

void EntityLogic::setGravity(float gravity)
{
	gravity_ = gravity;
}

void EntityLogic::setTerminalVelocity(const ne::Vector2f &terminal_velocity)
{
    terminal_velocity_ = terminal_velocity;
}

// Getters

const ne::Rectf& EntityLogic::getHitbox() const
{
    return hitbox_;
}

float EntityLogic::getX() const
{
    return hitbox_.x;
}

float EntityLogic::getY() const
{
    return hitbox_.y;
}

ne::Vector2f EntityLogic::getPosition() const
{
    return ne::Vector2f(hitbox_.x,hitbox_.y);
}

ne::Vector2f EntityLogic::getCentre() const
{
    return ne::Vector2f(hitbox_.x + hitbox_.w/2.0f, hitbox_.y + hitbox_.h/2.0f);
}

const ne::Vector2f& EntityLogic::getVelocity() const
{
   return velocity_;
}

const ne::Vector2f& EntityLogic::getStep() const
{
    return step_;
}

const ne::Vector2f& EntityLogic::getAcceleration() const
{
   return acceleration_;
}

const ne::Vector2f& EntityLogic::getForceVelocity() const
{
    return force_velocity_;
}

bool EntityLogic::getIgnoreGravity() const
{
	return ignore_gravity_;
}

float EntityLogic::getGravity() const
{
    return gravity_;
}

const ne::Vector2f& EntityLogic::getTerminalVelocity() const
{
    return terminal_velocity_;
}

}
