#include "Neru/Animation.h"

namespace ne
{

Animation::Animation()
{
    frame_ = 1;
    time_ = 0;
    loop_ = false;
    stop_ = false;
}

void Animation::animate(const float dt)
{
    if (timing_.empty() || stop_)
        return;

    time_ += dt*1000;
        
    // If the animation has exceeded the time of the frame
    if (time_ >= timing_[frame_-1])
    {
        // Subtract the time, add one to the frame count
        time_ -= timing_[frame_-1];
        frame_++;

        // If the frame exceeds the size
        if (frame_ > timing_.size())
        {
            // If the animation was meant to loop, set frame to 1
            if (loop_)
            {
                frame_ = 1;
                stop_ = false;
            }
            // Set frame to the last frame and set stop to true
            else
            {
                frame_--;
                stop_ = true;
            }
        }
    }
}

void Animation::insert(int ms)
{
    timing_.push_back(ms);
}

void Animation::reset()
{
	frame_ = 1;
    time_ = 0;
    stop_ = false;
}

void Animation::clean()
{
	frame_ = 1;
    stop_ = false;
    timing_.clear();
    time_ = 0;
}

// Setters

void Animation::setFrame(unsigned int frame)
{
    frame_ = frame;
}

void Animation::setTime(int ms)
{
    time_ = ms;
}

void Animation::setLoop(bool loop)
{
    loop_ = loop;
}

void Animation::setStop(bool stop)
{
    stop_ = stop;
}

// Getters

unsigned int Animation::getFrame() const
{
    return frame_;
}

int Animation::getTime() const
{
    return time_;
}

bool Animation::getLoop() const
{
    return loop_;
}

bool Animation::getStop() const
{
    return stop_;
}

}
