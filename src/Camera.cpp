#include "Neru/Camera.h"

namespace ne
{

Camera::Camera()
{
    lerp_speed_ = 8.0f;
}

Camera::Camera(int screen_w, int screen_h, int level_w, int level_h)
{
    level_size_.x = level_w;
    level_size_.y = level_h;
    screen_size_.x = screen_w;
    screen_size_.y = screen_h;
    lerp_speed_ = 8.0f;
}

void Camera::moveTo(const ne::Vector2f &target, const float dt)
{
    target_ = target;
    update(dt);
}

void Camera::snapTo(const ne::Vector2f &target)
{
    float x = target.x - offset_.x + screen_size_.x/2;
    float y = target.y - offset_.y + screen_size_.y/2;
    setPosition(ne::Vector2f(x,y));
}

void Camera::update(const float dt)
{
    last_position_ = position_;

    float distance_x = target_.x - offset_.x - position_.x;
    float distance_y = target_.y - offset_.y - position_.y;
    
    // Player is to the very left of the map
    if (target_.x < (screen_size_.x/2) + offset_.x)
    {
        distance_x = (screen_size_.x/2) - position_.x;
    }

    // Player is to the very right of the map
    if (target_.x > level_size_.x - (screen_size_.x/2) + offset_.x)
    {
        distance_x = level_size_.x - (screen_size_.x/2) - position_.x;
    } 

    // Player is at the very bottom of the map
    if (target_.y > level_size_.y - (screen_size_.y/2) + offset_.y)
    {
        distance_y = level_size_.y - (screen_size_.y/2) - position_.y;
    }

    // Player is at the very top of the map
    if (target_.y < (screen_size_.y/2) + offset_.y)
    {
        distance_y = (screen_size_.y/2) - position_.y;
    }

    float speed_x = distance_x/lerp_speed_;
    float speed_y = distance_y/lerp_speed_;

    position_.x = position_.x + speed_x;
    position_.y = position_.y + speed_y;
}

// Setters

void Camera::setPosition(const ne::Vector2f &position)
{
    float x = position.x - screen_size_.x/2;
    float y = position.y - screen_size_.y/2;

    // Player is at the very left of the map
    if (x < screen_size_.x/2 + offset_.x)
    {
        x = screen_size_.x/2;
    }

    // Player is at the very right of the map
    if (x > level_size_.x - (screen_size_.x/2) + offset_.x)
    {
        x = level_size_.x - (screen_size_.x/2);
    }

    // Player is at the bottom of the map
    if (y > level_size_.y - (screen_size_.y/2) + offset_.y)
    {
        y = level_size_.y - (screen_size_.y/2);
    }

    // Player is at the very top of the map
    if (y < (screen_size_.y/2) + offset_.y)
    {
        y = (screen_size_.y/2);
    }
    
    position_.x = x;
    position_.y = y;
    last_position_.x = x;
    last_position_.y = y;
}

void Camera::setScreenSize(const ne::Vector2i &screen_size)
{
    screen_size_ = screen_size;
}

void Camera::setLevelSize(const ne::Vector2i &level_size)
{
    level_size_ = level_size;
}

void Camera::setLerpSpeed(float lerp_speed)
{
    lerp_speed_ = lerp_speed;
}

void Camera::setOffset(const ne::Vector2f &offset)
{
    offset_ = offset;
}

// Getters

ne::Vector2f Camera::getTranslation(const float interpolation) const
{
    ne::Vector2f pos;
    pos.x = position_.x*interpolation + last_position_.x*(1.0f - interpolation) - screen_size_.x/2;
    pos.y = position_.y*interpolation + last_position_.y*(1.0f - interpolation) - screen_size_.y/2;
    return pos;
}

ne::Vector2f Camera::getPosition() const
{
    return ne::Vector2f(position_.x + screen_size_.x/2, position_.y + screen_size_.y/2); 
}

const ne::Vector2i& Camera::getScreenSize() const
{
    return screen_size_;
}

const ne::Vector2i& Camera::getLevelSize() const
{
    return level_size_;
}

float Camera::getLerpSpeed() const
{
    return lerp_speed_;
}

const ne::Vector2f& Camera::getOffset() const
{
    return offset_;
}

}

