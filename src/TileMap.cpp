#include "Neru/TileMap.h"

namespace ne
{

TileMap::TileMap()
{

}

void TileMap::loadFromFile(const std::string &map_file, const std::string &texture_path)
{
    texture_.loadFileImage(texture_path);
    parseFile(map_file);
    texture_.setSpriteSheet(getTileSize().x,getTileSize().y,getSpacing());
}

void TileMap::loadFromZip(const std::string &map_file, const std::string &texture_path)
{
    texture_.loadZipImage(texture_path);
    parseZip(map_file);
    texture_.setSpriteSheet(getTileSize().x,getTileSize().y,getSpacing());
}

// Private

void TileMap::setRenderTile(int tile_id)
{
    int row = (tile_id / texture_.getSpritesPerRow()) + 1;
    int col = (tile_id - (row-1)*texture_.getSpritesPerRow()) + 1;
    texture_.setSprite(row,col);
}

// Getters

SDL_Texture* TileMap::getTexture()
{
    return texture_.getTexture();
}

SDL_Rect* TileMap::getTextureRect()
{
    return texture_.getTextureRect();
}

ne::Vector2i TileMap::getPixelSize() const
{
    return ne::Vector2i(getTileSize().x*getSize().x, getTileSize().y*getSize().y);
}

ne::SpriteSheet* TileMap::getSpriteSheet()
{
    return &texture_;
}

}
