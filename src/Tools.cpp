#include "Neru/Tools.h"

namespace ne
{

std::string toString(bool boolean)
{
    return boolean ? "true" : "false";
}

std::string toString(char character)
{
   std::stringstream ss;
   ss << character;
   return ss.str();	
}

std::string toString(float number)
{
   std::stringstream ss;
   ss << std::fixed << number;
   return ss.str();	
}

std::string toString(int number)
{
   std::stringstream ss;
   ss << number;
   return ss.str();	
}

std::string toString(double number)
{
   std::stringstream ss;
   ss << number;
   return ss.str();	
}

std::string toString(long number)
{
   std::stringstream ss;
   ss << number;
   return ss.str();
}

std::string toString(const ne::Vector2f &vector)
{
   std::stringstream ss;
   ss << vector.x << "," << vector.y;
   return ss.str();
}

std::string toString(const ne::Vector2i &vector)
{
   std::stringstream ss;
   ss << vector.x << "," << vector.y;
   return ss.str();
}

bool fileExists(const std::string &path)
{
    std::ifstream infile(path.c_str());
    return infile.good();
}

float absval(float number)
{
    return number < 0 ? number * -1 : number;
}

int round(float num)
{
	return num + 0.5f;
}

int random(int min, int max)
{
    return ((static_cast<double>(std::rand())/RAND_MAX) * (max - min + 1)) + min;
}

float random(float min, float max, int precision)
{
    int mi = min * precision;
    int ma = max * precision;
    return random(mi, ma) / static_cast<float>(precision);
}

bool intersects(const ne::Rectf &A, const ne::Rectf &B)
{
	if(A.y + A.h <= B.y) return false;
    if(A.y >= B.y + B.h) return false;
    if(A.x + A.w <= B.x) return false;
    if(A.x >= B.x + B.w) return false;
    
    return true;
}

bool intersects(const ne::Recti &A, const ne::Recti &B)
{
	if(A.y + A.h <= B.y) return false;
    if(A.y >= B.y + B.h) return false;
    if(A.x + A.w <= B.x) return false;
    if(A.x >= B.x + B.w) return false;
    
    return true;
}

// Taken from SFML user Disch
bool intersects(const ne::Vector2f &A1, const ne::Vector2f &A2, const ne::Vector2f &B1, const ne::Vector2f &B2, ne::Vector2f *intersection)
{
    ne::Vector2f a(A2-A1);
    ne::Vector2f b(B2-B1);
    ne::Vector2f c(B2-A2);

    float f = (a.y * b.x) - (a.x * b.y);
    if (f == 0)
        return false;

    float aa = (a.y * c.x) - (a.x * c.y);
    float bb = (b.y * c.x) - (b.x * c.y);

    if (f < 0)
    {
        if (aa > 0) return false;
        if (bb > 0) return false;
        if (aa < f) return false;
        if (bb < f) return false;
    }
    else
    {
        if (aa < 0) return false;
        if (bb < 0) return false;
        if (aa > f) return false;
        if (bb > f) return false;
    }

    if (intersection != NULL)
        *intersection = b * (1.0f - (aa / f)) + B1;

    return true;
}

// From zacharmarz @ stackexchange.com
// http://gamedev.stackexchange.com/questions/18436/most-efficient-aabb-vs-ray-collision-algorithms 
bool intersects(const ne::Vector2f &ray_position, ne::Vector2f ray_direction, const ne::Rectf &square, float *distance)
{
    ray_direction.normalize();

    ne::Vector2f frac;
    frac.x = 1.0f / ray_direction.x;
    frac.y = 1.0f / ray_direction.y;

    // lb is the corner of AABB with minimal coordinates - left bottom, rt is maximal corner
    // r.org is origin of ray
    ne::Vector2f lb;
    lb.x = square.x;
    lb.y = square.y;

    ne::Vector2f rt;
    rt.x = square.x + square.w;
    rt.y = square.y + square.h;

    float t1 = (lb.x - ray_position.x)*frac.x;
    float t2 = (rt.x - ray_position.x)*frac.x;
    float t3 = (lb.y - ray_position.y)*frac.y;
    float t4 = (rt.y - ray_position.y)*frac.y;

    float tmin = std::max(std::min(t1, t2), std::min(t3, t4));
    float tmax = std::min(std::max(t1, t2), std::max(t3, t4));

    // if tmax < 0, ray (line) is intersecting AABB, but whole AABB is behing us
    if (tmax < 0)
    {
        if (distance != NULL)
            *distance = tmax;

        return false;
    }

    // if tmin > tmax, ray doesn't intersect AABB
    if (tmin > tmax)
    {
        if (distance != NULL)
            *distance = tmax;

        return false;
    }

    if (distance != NULL)
        *distance = tmin;

    return true;
}

ne::Recti toRect(const SDL_Rect &rect)
{
    ne::Recti temp;
    temp.x = rect.x;
    temp.y = rect.y;
    temp.w = rect.w;
    temp.h = rect.h;
    return temp;
}

SDL_Rect toSDLRect(const ne::Recti &rect)
{
    SDL_Rect temp;
    temp.x = rect.x;
    temp.y = rect.y;
    temp.w = rect.w;
    temp.h = rect.h;
    return temp;
}

SDL_Rect toSDLRect(int x, int y, int w, int h)
{
    SDL_Rect temp;
    temp.x = x;
    temp.y = y;
    temp.w = w;
    temp.h = h;
    return temp;
}

SDL_Color toSDLColor(int r, int g, int b, int a)
{
    if (r > 255) r = 255;
    if (g > 255) g = 255;
    if (b > 255) b = 255;
    if (a > 255) a = 255;
    if (r < 0) r = 0;
    if (g < 0) g = 0;
    if (b < 0) b = 0;
    if (a < 0) a = 0;
    
    SDL_Color temp;
    temp.r = r;
    temp.g = g;
    temp.b = b;
    temp.a = a;
    
    return temp;
}

}
